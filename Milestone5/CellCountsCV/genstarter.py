import os

scripts = [f[:-3] for f in os.listdir() if f[0] != '.' and f[-3:] == '.py'
           and f not in ['genjobscript.py', 'genstarter.py']]

with open('submit.bash', 'w') as fout:
    fout.write('for script in ' + ' '.join(scripts) + '\n')
    fout.write('do\n')
    fout.write('\tqsub "$script".job\n')
    fout.write('done')
