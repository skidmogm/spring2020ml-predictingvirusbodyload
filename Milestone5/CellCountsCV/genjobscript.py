import os

scripts = [f[:-3] for f in os.listdir() if f[0] != '.' and f[-3:] == '.py'
           and f not in ['genjobscript.py', 'genstarter.py']]

for script in scripts:
    with open(f'{script}.job', 'w') as fout:
        fout.writelines([
            '#!/bin/bash -l\n',
            '#PBS -N ' + script + '\n',
            '#PBS -l nodes=1:ppn=4\n',
            '#PBS -l walltime=48:00:00\n',
            '#PBS -q batch\n',
            '#PBS -m abe\n',
            '#PBS -M lij111@miamioh.edu\n',
            '#PBS -j oe\n',
            '\n',
            'cd /shared/giabbapj_shared/spring2020ml-predictingvirusbodyload/Milestone5/CellCountsCV/\n',
            'source /software/python/anaconda3/etc/profile.d/conda.sh\n',
            f'$CONDA_PYTHON_EXE {script}.py'
        ])
