import os
BASE_DIR = os.getcwd()
times = ['2', '4', '8']

for time in times:
    os.chdir(os.path.join(BASE_DIR, time))
    scripts = [f[:-3] for f in os.listdir() if f[0] != '.' and f[-3:] == '.py']
    
    with open('submit.bash', 'w') as fout:
        fout.write('for script in ' + ' '.join(scripts) + '\n')
        fout.write('do\n')
        fout.write('\tqsub "$script".job\n')
        fout.write('done')

