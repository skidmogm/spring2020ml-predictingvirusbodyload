from sklearn.model_selection import ParameterGrid, KFold
from sklearn.gaussian_process import GaussianProcessRegressor as gpr
from sklearn.gaussian_process.kernels import RBF as rbf
from sklearn.gaussian_process.kernels import RationalQuadratic as rq
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np
from IPython.display import clear_output
from itertools import product
import os

from concurrent.futures import ProcessPoolExecutor


def load_data(model):
    """ Helper function to load processed spreadsheet.

    This function drops useless columns and converts all cell counts
    to percentages.
    """

    data = pd.read_csv('clean_data/{}.csv'.format(model))

    # Convert cell counts to percentages
    data['_N_CELLS'] = data['L'] ** 2
    for col in ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']:
        data[col] = data[col] / data['_N_CELLS']

    # Drop useless columns. Incidentally, CELL_N is always zero
    # for all 900 runs for the moonchai model, so we drop that
    # column as well
    data = data.drop(['Model', 'Run', 'L', 'CELL_N', '_N_CELLS'], axis=1)

    return data


def nested_cv(X, y, inner_cv, outer_cv, Classifier, kernel, parameter_grid):
    outer_scores = []  # List of RMSE scores
    params_out = []  # List saving the best_params for each outer fold

    for training_samples, test_samples in outer_cv.split(X, y):
        best_params = {}
        best_score = -np.inf
        for parameters in parameter_grid:
            cv_scores = []
            for inner_train, inner_test in inner_cv.split(
                    X.loc[training_samples], y[training_samples]):
                kern = kernel(**parameters)
                clf = Classifier(kern)

                # Scale data
                scaler_X, scaler_y = StandardScaler(), StandardScaler()
                train_X = scaler_X.fit_transform(X.loc[inner_train])
                train_y = scaler_y.fit_transform(
                    np.array(y.loc[inner_train]).reshape(-1, 1))
                test_X = scaler_X.transform(X.loc[inner_test])
                test_y = scaler_y.transform(
                    np.array(y.loc[inner_test]).reshape(-1, 1))

                clf.fit(train_X, train_y)
                score = clf.score(test_X, test_y)
                cv_scores.append(score)
            mean_score = np.mean(cv_scores)
            if mean_score > best_score:
                best_score = mean_score
                best_params = parameters
        params_out.append(best_params)
        kern = kernel(**best_params)
        clf = Classifier(kern)

        # Scale data
        scaler_X, scaler_y = StandardScaler(), StandardScaler()
        train_X = scaler_X.fit_transform(X.loc[training_samples])
        train_y = scaler_y.fit_transform(
            np.array(y.loc[training_samples]).reshape(-1, 1))
        test_X = scaler_X.transform(X.loc[test_samples])
        test_y = scaler_y.transform(
            np.array(y.loc[test_samples]).reshape(-1, 1))

        clf.fit(train_X, train_y)
        y_predict = clf.predict(test_X)
        outer_scores.append(
            np.sqrt(np.mean((scaler_y.inverse_transform(test_y) - scaler_y.inverse_transform(y_predict)) ** 2)))

    return outer_scores, params_out


def thread_main(model):
    data = load_data(model)
    y_cols = ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']
    X_cols = [col for col in data.columns if col not in y_cols]

    # Open file
    fout = open('gaussian_process/{}.csv'.format(model), 'w')

    # Headers to write
    headers = ['Model', 'Class', 'T_bound']
    headers.extend(['score' + str(i) for i in range(1, 11)])
    headers.extend(['score_avg', 'score_stdev'])
    headers.extend([head + str(i) for i in range(1, 11)
                    for head in ['alpha', 'length_scale']])
    fout.write(','.join(headers) + '\n')
    fout.flush()

    # Parameter grids
    # params_rq = ParameterGrid(
    #             {'alpha': [1e-4, 1e-2, 1, 100.0],
    #              'length_scale': [1e-4, 1e-2, 1, 100.0]})
    params_rbf = ParameterGrid(
        {'length_scale': [1e-4, 1e-2, 1, 100.0, 10000.0]})

    for y in y_cols:
        for t in range(1, 42):
            this_X_cols = X_cols[:20*t]
            scores, param = nested_cv(data[this_X_cols], data[y], KFold(
                shuffle=True, n_splits=10), KFold(shuffle=True, n_splits=10),
                gpr, rbf, params_rbf)
            row = [model, y, 5 * (t - 1)]
            row.extend([scores[i] for i in range(10)])
            row.extend([np.average(scores), np.std(scores)])
            row.extend([param[i][k] for i in range(10)
                        for k in ['length_scale']])
            row = list(map(lambda x: str(x), row))
            fout.write(','.join(row) + '\n')
            fout.flush()
    # for y in y_cols:
    #     for t in range(1, 42):
    #         this_X_cols = X_cols[:20*t]
    #         scores, param = nested_cv(data[this_X_cols], data[y], KFold(n_splits=10), KFold(n_splits=10),
    #                                   gpr, rq, params_rq)
    #         row = [model, y, 5 * (t - 1)]
    #         row.extend([scores[i] for i in range(10)])
    #         row.extend([np.average(scores), np.std(scores)])
    #         row.extend([param[i][k] for i in range(10)
    #                     for k in ['alpha', 'length_scale']])
    #         row = list(map(lambda x: str(x), row))
    #         fout.write(','.join(row) + '\n')
    #         fout.flush()


if __name__ == "__main__":
    thread_main('rana')
