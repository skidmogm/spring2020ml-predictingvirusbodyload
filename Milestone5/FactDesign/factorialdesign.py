import pandas as pd
import numpy as np
import sys
from sklearn.model_selection import KFold, ParameterGrid
from sklearn.tree import DecisionTreeRegressor

from concurrent.futures import ProcessPoolExecutor
from itertools import product

# Define a few constants
blobs_features = ['10_num_blobs', '10_avg_blob_dist', '10_avg_blob_size']
contour_10_features = ['cnt_area_10', 'avg_area_10', 'max_cnt_area']
contour_100_features = ['cnt_area_100', 'avg_area_100', 'max_cnt_area']
contour_perim_10_features = ['cnt_perim_10', 'avg_perim_10', 'max_cnt_perim']
contour_perim_20_features = ['cnt_perim_20',  'avg_perim_20', 'max_cnt_perim']
logblob_features = ['numofLogBlob',
                    'AvgSizeofLogblob', 'blob_varx', 'blob_vary']
corners_features = ['Num Corners', 'Avg Manhatten Dist Between Corners',
                    'Avg Euclidean Dist Between Corners']
all_features = [blobs_features, contour_10_features, contour_100_features, contour_perim_10_features,
                contour_perim_20_features, logblob_features, corners_features]


def load_data(model):
    """ Helper function to load processed spreadsheet.

    This function drops useless columns and converts all cell counts
    to percentages.
    """

    data = pd.read_csv('data/{}.csv'.format(model))

    # Convert cell counts to percentages
    data['_N_CELLS'] = data['L'] ** 2
    for col in ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']:
        data[col] = data[col] / data['_N_CELLS']

    # Drop useless columns. Incidentally, CELL_N is always zero
    # for all 900 runs for the moonchai model, so we drop that
    # column as well
    data = data.drop(['Model', 'Run', 'L', 'CELL_N', '_N_CELLS'], axis=1)

    return data


def nested_cv(X, y, inner_cv, outer_cv, Classifier, parameter_grid):
    outer_scores = []  # List of RMSE scores
#     params_out = []  # List saving the best_params for each outer fold

    for training_samples, test_samples in outer_cv.split(X, y):
        best_params = {}
        best_score = -np.inf
        for parameters in parameter_grid:
            cv_scores = []
            for inner_train, inner_test in inner_cv.split(
                    X.loc[training_samples], y[training_samples]):
                clf = Classifier(**parameters)
                clf.fit(X.loc[inner_train], y[inner_train])
                score = clf.score(X.loc[inner_test], y[inner_test])
                cv_scores.append(score)
            mean_score = np.mean(cv_scores)
            if mean_score > best_score:
                best_score = mean_score
                best_params = parameters
#         params_out.append(best_params)
        clf = Classifier(**best_params)
        clf.fit(X.loc[training_samples], y[training_samples])
#         outer_scores.append(clf.score(X.loc[test_samples], y[test_samples]))
        y_predict = clf.predict(X.loc[test_samples])
        outer_scores.append(
            np.sqrt(np.mean((y[test_samples] - y_predict) ** 2)))

    return outer_scores


def thread_main(tup):
    """
    Function to perform one nested cv of the factorial analysis. Accepts config
    as a string of binary numbers, indicating which batch of features to use.
    Also accepts model to indicate which model to run. Both parameters are
    encapsulated in a tuple.
    """
    print('called with', tup)
    config, model = tup

    # Find all the features we need to keep from the config string
    features = [f for i, c in enumerate(config)
                for f in all_features[i] if c == '1']

    # For each feature we keep, append it with T=0 up to T=200 in intervals of 5
    X_cols = [f'T={time}_{feat}' for time in range(
        0, 201, 5) for feat in features]
    y_cols = ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']

    data = load_data(model)
    data.drop([f for f in data.columns if f not in X_cols and f not in y_cols],
              inplace=True, axis=1)

    params = ParameterGrid({'max_depth': [8], 'min_samples_split': [8]})

    scores = {k: 0 for k in y_cols}
    for y in y_cols:
        scores[y] = np.average(
            nested_cv(data[X_cols], data[y], KFold(shuffle=True, n_splits=10),
                      KFold(shuffle=True, n_splits=10), DecisionTreeRegressor,  params))

    # print('returning', scores)
    return scores


models = ['rana', 'gonzalez', 'moonchai', 'precharattana', 'dossantos']
#models = ['rana', 'gonzalez']

if __name__ == '__main__':
    configs = [format(i, 'b').rjust(7, '0') for i in range(1, 128)]
    params = list(product(configs, models))

    # with ProcessPoolExecutor(max_workers=12) as exe:
    #     res = list(exe.map(thread_main, params))

    # with open('factdesign.csv', 'w') as fout:
    #     headers = ['model', 'blobs_features', 'contour_10_features', 'contour_100_features',
    #                'contour_perim_10_features', 'contour_perim_20_features', 'logblob_features', 'corners_features',
    #                'CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']
    #     fout.write(','.join(headers) + '\n')
    #     for param, result in zip(params, res):
    #         config, model = param
    #         line = ['1' if c == '1' else '-1' for c in config]
    #         line = [model, *line, *[str(result[k])
    #                                 for k in ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']]]
    #         fout.write(','.join(line) + '\n')

    futures = []
    with ProcessPoolExecutor(max_workers=12) as exe:
        for param in params:
            futures.append(exe.submit(thread_main, param))

    with open('factdesign.csv', 'w') as fout:
        headers = ['model', 'blobs_features', 'contour_10_features', 'contour_100_features',
                   'contour_perim_10_features', 'contour_perim_20_features', 'logblob_features', 'corners_features',
                   'CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']
        fout.write(','.join(headers) + '\n')
        for param, ftr in zip(params, futures):
            config, model = param
            result = ftr.result()
            line = ['1' if c == '1' else '-1' for c in config]
            line = [model, *line, *[str(result[k])
                                    for k in ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']]]
            fout.write(','.join(line) + '\n')
