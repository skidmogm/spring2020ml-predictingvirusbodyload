{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3 Data Cleaning\n",
    "In this notebook we summarize all the data cleaning scripts employed in this project. In particular, this script will grab and clean data in a specified directory based on the naming scheme we employed throughout the project. For completeness and future reference, the names and files are explained below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Naming Schemes\n",
    "As always, we have 5 models, usually denoted by `rana`, `dossantos`, `gonzalez`, `precharattana`, and `moonchai`. We ran each simulation model 300 times for grid sizes $800\\times800$, $1000\\times1000$, and $1200\\times1200$.\n",
    "\n",
    "### Simulation Cell Counts\n",
    "While running each model, we also recorded the count of each cell type at the very last time step of the simulation, and compiled them to a csv file. These simulation cell counts are given in the following files --\n",
    "* `ranaout.csv`\n",
    "* `dossantosout_800.csv`, `dossantosout_1000.csv`, `dossantosout_1200.csv`\n",
    "* `gonzalezout.csv`\n",
    "* `moonchaiout.csv`\n",
    "* `precharattana_800.csv`, `precharattana_1000.csv`, `precharattana_1200.csv`\n",
    "\n",
    "For some of the models, data are split further by grid size since we ran the scripts in parallel.\n",
    "\n",
    "### Extracted Features\n",
    "While running each model, we also saved the grid at each time step. We later then ran a feature extraction script over these saved scripts to get a csv of image data. Some fields are missing because they are discarded due to unreasonable running times (5 mins for one \"cell\" in the csv, no thanks). The raw output of these scripts are in the following file format -- `[model name]_[grid size]_grid_features.csv`, for example, `rana_800_grid_features.csv`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Cleaning Code\n",
    "We need to process both the simulation cell counts files and the extracted feature files. \n",
    "\n",
    "For the simulation cell counts files, we need to condense all columns into 5 \"aggregate\" types, healthy (`CELL_H`), dead (`CELL_D`), acute infected (`CELL_A1`), latent infected (`CELL_A2`), and `CELL_N`. The latter type is unused but is kept here for consistency with later scripts.\n",
    "\n",
    "In the extracted feature files, there are empty columns (as discussed above), so we need to drop them. Furthermore, for each run of each model given a fixed size, there are 41 rows corresponding to the features extracted from the simulation, each row containing all features extracted at one time step. We will group all 41 rows into one row by creating more columns (`T=0_feature_A`, `T=0_feature_B`, ..., `T=5_feature_A`, ...).\n",
    "\n",
    "Finally, we join these two data sheets together by model, run, and grid size to form the standard csv used in machine learning algorithms, with all features and all attributes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initializations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import os"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# RUN THE CELL BELOW ONLY ONCE IN EACH SESSION!\n",
    "BASE_DIR = os.getcwd()\n",
    "DATA_DIR = os.path.join(BASE_DIR, os.pardir, os.pardir, 'Data', 'Gathered')\n",
    "\n",
    "SIM_DATA_DIR = os.path.join(DATA_DIR, 'sim_cell_count')\n",
    "FEATURE_DATA_DIR = os.path.join(DATA_DIR, 'features')\n",
    "CELL_COUNT_DATA_DIR = os.path.join(DATA_DIR, 'all_cell_count')\n",
    "\n",
    "SAVE_DIR = os.path.join(BASE_DIR, os.pardir, os.pardir, 'Data', 'Cleaned')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "models = ['rana', 'gonzalez', 'precharattana', 'dossantos', 'moonchai']\n",
    "pretty_name = {'rana': 'Rana', 'dossantos': 'Dos Santos', 'gonzalez': 'Gonzalez',\n",
    "              'moonchai': 'Moonchai', 'precharattana': 'Precharattana'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulation Cell Counts Cleaning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# In this cell, we load into memory and process all the simulation cell count data\n",
    "# All the saved results we be stored in the following dictionary\n",
    "os.chdir(SIM_DATA_DIR)\n",
    "cleaned_sim_cell_counts = {}\n",
    "\n",
    "for model in models:\n",
    "    # Load the data as appropriate\n",
    "    data = None\n",
    "    if model == 'dossantos':\n",
    "        dfs = []\n",
    "        for L in [800, 1000, 1200]:\n",
    "            fname = f'dossantosout_{L}.csv'\n",
    "            dfs.append(pd.read_csv(fname))\n",
    "        data = pd.concat(dfs).reset_index(drop=True)\n",
    "    elif model == 'precharattana':\n",
    "        dfs = []\n",
    "        for L in [800, 1000, 1200]:\n",
    "            fname = f'precharattana_{L}.csv'\n",
    "            dfs.append(pd.read_csv(fname))\n",
    "        data = pd.concat(dfs).reset_index(drop=True)\n",
    "    else:\n",
    "        data = pd.read_csv(f'{model}out.csv')\n",
    "    \n",
    "    # In a few instances we saved the incorrect model name into the column,\n",
    "    # but the filename indicates which model it really belongs. We fix this here.\n",
    "    data['Model'] = [pretty_name[model]] * len(data)\n",
    "    \n",
    "    # Enumerate all the feature columns\n",
    "    num_cols = [col for col in data.columns if col not in ['Model', 'Run', 'L']]\n",
    "    \n",
    "    # Convert missing to 0\n",
    "    data[num_cols] = data[num_cols].applymap(lambda x : 0 if np.isnan(x) else x)\n",
    "    \n",
    "    # From the current dataframe, build a new dataframe as the result\n",
    "    data_out = data[['Model', 'Run', 'L']].copy()\n",
    "    for col in ['CELL_H', 'CELL_N', 'CELL_A1', 'CELL_A2', 'CELL_D']:\n",
    "        data_out[col] = data[col]\n",
    "    for col in data.columns:\n",
    "        col = str(col)\n",
    "        # Lump all the healthy states into one.\n",
    "        if col.find('CELL_H') != -1 and not col == 'CELL_H':\n",
    "            data_out['CELL_H'] += data[col]\n",
    "    \n",
    "    # The numbering scheme for precharattana model is a bit different\n",
    "    if model == 'precharattana':\n",
    "        data_out['CELL_A2'] = data['CELL_A0']\n",
    "    \n",
    "    cleaned_sim_cell_counts[model] = data_out.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extracted Features Cleaning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This cell contains a feature that will merge the rows as described.\n",
    "def proc_features(model, raw_data):    \n",
    "    # Separate raw_data in to a list of dataframes, each only containing only\n",
    "    # one time stamp. (I.e., the t=0 features for all runs, the t=5 features \n",
    "    # of all runs, etc.)\n",
    "    subsets = []\n",
    "    for t in range(0, 201, 5):\n",
    "        subset_ind = [i for i in raw_data.index if i % 41 == (t // 5)]\n",
    "        subsets.append(raw_data.loc[subset_ind].copy())\n",
    "    \n",
    "    # Rename all feature columns to tag a \"T=t_\" in front\n",
    "    for df, t in zip(subsets, range(0, 201, 5)):\n",
    "        new_names = {col:'T={}_{}'.format(t, col) for col in df.columns[4:]}\n",
    "        df.rename(columns=new_names, inplace=True)\n",
    "    \n",
    "    # Reset index for all dataframes, and drop the T column\n",
    "    for df in subsets:\n",
    "        df.reset_index(drop=True, inplace=True)\n",
    "        df.drop('T', axis=1, inplace=True)\n",
    "    \n",
    "    # Drop all the ['Model', 'L', 'Run'] columns of all but the first\n",
    "    # dataframe in subsets.\n",
    "    for df in subsets[1:]:\n",
    "        df.drop(['Model', 'L', 'Run'], axis=1, inplace=True)\n",
    "    \n",
    "    # Merge all the dataframes\n",
    "    res = subsets[0].copy()\n",
    "    for df in subsets[1:]:\n",
    "        res = res.join(df, how='inner')\n",
    "\n",
    "    return res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# In this cell, we process the feature extraction data as prescribed.\n",
    "os.chdir(FEATURE_DATA_DIR)\n",
    "cleaned_feature = {}\n",
    "\n",
    "for model in models:\n",
    "    # Read all parts of the data\n",
    "    dfs = []\n",
    "    for L in [800, 1000, 1200]:\n",
    "        fname = f'{model}_{L}_grid_features.csv'\n",
    "        dfs.append(pd.read_csv(fname))\n",
    "    data = pd.concat(dfs).reset_index(drop=True)\n",
    "    data['Model'] = [pretty_name[model]] * len(data)\n",
    "    \n",
    "    # Some of these were written a value 0, but these features are really\n",
    "    # meant to be dropped\n",
    "    data.drop('Avg Manhatten Dist Between Blobs', inplace=True, axis=1)\n",
    "    data.drop('Avg Euclidean Dist Between Blobs', inplace=True, axis=1)\n",
    "    data.dropna(axis=1, inplace=True)\n",
    "    \n",
    "    data = data[['Model', *data.columns[:-1]]]\n",
    "    data = proc_features(model, data.copy())\n",
    "    \n",
    "    cleaned_feature[model] = data.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### All Cell Counts Cleaning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir(CELL_COUNT_DATA_DIR)\n",
    "cleaned_cell_counts = {}\n",
    "\n",
    "for model in models:\n",
    "    data = pd.read_csv(f'{model}_cell_counts.csv')\n",
    "    if model != 'moonchai':\n",
    "        data['CELL_N'] = [0] * len(data)\n",
    "    data['Model'] = [pretty_name[model]] * len(data)\n",
    "    data = proc_features(model, data.copy())\n",
    "    cleaned_cell_counts[model] = data.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Merging"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.chdir(SAVE_DIR)\n",
    "for model in models:\n",
    "    counts = cleaned_sim_cell_counts[model]\n",
    "    features = cleaned_feature[model]\n",
    "    allcounts = cleaned_cell_counts[model]\n",
    "    \n",
    "    features.drop(['Model', 'L', 'Run'], axis=1, inplace=True)\n",
    "    allcounts.drop(['Model', 'L', 'Run'], axis=1, inplace=True)\n",
    "    \n",
    "    counts.join(features, how='outer').to_csv(f'{model}.csv', index=False)\n",
    "    counts.join(allcounts, how='outer').to_csv(f'{model}_cell_counts.csv', index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Cleaning Code for Early Treament\n",
    "Large sections of this code will be similar if not identical to the previous cells. However, there are slight modifications. It is not worth the hassle to encapsulate the code into functions, so we just repeat them here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "models = ['rana', 'gonzalez']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# In this cell, we load into memory and process all the simulation cell count data\n",
    "# All the saved results we be stored in the following dictionary\n",
    "for treatment_time in ['2', '4']:\n",
    "    # The directories will change\n",
    "    DATA_DIR = os.path.join(BASE_DIR, os.pardir, os.pardir, 'Data', 'Gathered', \n",
    "                            'EarlyTreatment', treatment_time)\n",
    "    SIM_DATA_DIR = os.path.join(DATA_DIR, 'sim_cell_count')\n",
    "    FEATURE_DATA_DIR = os.path.join(DATA_DIR, 'features')\n",
    "    CELL_COUNT_DATA_DIR = os.path.join(DATA_DIR, 'all_cell_count')\n",
    "    SAVE_DIR = os.path.join(BASE_DIR, os.pardir, os.pardir, 'Data', 'Cleaned',\n",
    "                           'EarlyTreatment', treatment_time)\n",
    "    \n",
    "    # Processing the Simulation Cell Counts\n",
    "    os.chdir(SIM_DATA_DIR)\n",
    "    cleaned_sim_cell_counts = {}\n",
    "    \n",
    "    for model in models:\n",
    "        data = pd.read_csv(f'{model}out.csv')\n",
    "\n",
    "        # In a few instances we saved the incorrect model name into the column,\n",
    "        # but the filename indicates which model it really belongs. We fix this here.\n",
    "        data['Model'] = [pretty_name[model]] * len(data)\n",
    "\n",
    "        # Enumerate all the feature columns\n",
    "        num_cols = [col for col in data.columns if col not in ['Model', 'Run', 'L']]\n",
    "\n",
    "        # Convert missing to 0\n",
    "        data[num_cols] = data[num_cols].applymap(lambda x : 0 if np.isnan(x) else x)\n",
    "\n",
    "        # From the current dataframe, build a new dataframe as the result\n",
    "        data_out = data[['Model', 'Run', 'L']].copy()\n",
    "        for col in ['CELL_H', 'CELL_N', 'CELL_A1', 'CELL_A2', 'CELL_D']:\n",
    "            data_out[col] = data[col]\n",
    "        for col in data.columns:\n",
    "            col = str(col)\n",
    "            # Lump all the healthy states into one.\n",
    "            if col.find('CELL_H') != -1 and not col == 'CELL_H':\n",
    "                data_out['CELL_H'] += data[col]\n",
    "\n",
    "        cleaned_sim_cell_counts[model] = data_out.copy()\n",
    "        \n",
    "    # We process the feature extraction data as prescribed.\n",
    "    os.chdir(FEATURE_DATA_DIR)\n",
    "    cleaned_feature = {}\n",
    "\n",
    "    for model in models:\n",
    "        # Read all parts of the data\n",
    "        dfs = []\n",
    "        for L in [800, 1000, 1200]:\n",
    "            fname = f'{model}_{L}_grid_features.csv'\n",
    "            dfs.append(pd.read_csv(fname))\n",
    "        data = pd.concat(dfs).reset_index(drop=True)\n",
    "        data['Model'] = [pretty_name[model]] * len(data)\n",
    "\n",
    "        # Some of these were written a value 0, but these features are really\n",
    "        # meant to be dropped\n",
    "        data.drop('Avg Manhatten Dist Between Blobs', inplace=True, axis=1)\n",
    "        data.drop('Avg Euclidean Dist Between Blobs', inplace=True, axis=1)\n",
    "        data.dropna(axis=1, inplace=True)\n",
    "\n",
    "        data = data[['Model', *data.columns[:-1]]]\n",
    "        data = proc_features(model, data.copy())\n",
    "\n",
    "        cleaned_feature[model] = data.copy()\n",
    "    \n",
    "    # Process all cell counts\n",
    "    os.chdir(CELL_COUNT_DATA_DIR)\n",
    "    cleaned_cell_counts = {}\n",
    "\n",
    "    for model in models:\n",
    "        data = pd.read_csv(f'{model}_cell_counts.csv')\n",
    "        if model != 'moonchai':\n",
    "            data['CELL_N'] = [0] * len(data)\n",
    "        data['Model'] = [pretty_name[model]] * len(data)\n",
    "        data = proc_features(model, data.copy())\n",
    "        cleaned_cell_counts[model] = data.copy()\n",
    "    \n",
    "    # Merge\n",
    "    os.chdir(SAVE_DIR)\n",
    "    for model in models:\n",
    "        counts = cleaned_sim_cell_counts[model]\n",
    "        features = cleaned_feature[model]\n",
    "        allcounts = cleaned_cell_counts[model]\n",
    "\n",
    "        features.drop(['Model', 'L', 'Run'], axis=1, inplace=True)\n",
    "        allcounts.drop(['Model', 'L', 'Run'], axis=1, inplace=True)\n",
    "        \n",
    "        counts.join(features, how='outer').to_csv(f'{model}.csv', index=False)\n",
    "        counts.join(allcounts, how='outer').to_csv(f'{model}_cell_counts.csv', index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Temporary Special Treatment for 8\n",
    "We only do the allcounts part since that is ready. We are still waiting on feature extraction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The directories will change\n",
    "treatment_time = '8'\n",
    "DATA_DIR = os.path.join(BASE_DIR, os.pardir, os.pardir, 'Data', 'Gathered', \n",
    "                        'EarlyTreatment', treatment_time)\n",
    "SIM_DATA_DIR = os.path.join(DATA_DIR, 'sim_cell_count')\n",
    "FEATURE_DATA_DIR = os.path.join(DATA_DIR, 'features')\n",
    "CELL_COUNT_DATA_DIR = os.path.join(DATA_DIR, 'all_cell_count')\n",
    "SAVE_DIR = os.path.join(BASE_DIR, os.pardir, os.pardir, 'Data', 'Cleaned',\n",
    "                       'EarlyTreatment', treatment_time)\n",
    "\n",
    "# Processing the Simulation Cell Counts\n",
    "os.chdir(SIM_DATA_DIR)\n",
    "cleaned_sim_cell_counts = {}\n",
    "\n",
    "for model in models:\n",
    "    data = pd.read_csv(f'{model}out.csv')\n",
    "\n",
    "    # In a few instances we saved the incorrect model name into the column,\n",
    "    # but the filename indicates which model it really belongs. We fix this here.\n",
    "    data['Model'] = [pretty_name[model]] * len(data)\n",
    "\n",
    "    # Enumerate all the feature columns\n",
    "    num_cols = [col for col in data.columns if col not in ['Model', 'Run', 'L']]\n",
    "\n",
    "    # Convert missing to 0\n",
    "    data[num_cols] = data[num_cols].applymap(lambda x : 0 if np.isnan(x) else x)\n",
    "\n",
    "    # From the current dataframe, build a new dataframe as the result\n",
    "    data_out = data[['Model', 'Run', 'L']].copy()\n",
    "    for col in ['CELL_H', 'CELL_N', 'CELL_A1', 'CELL_A2', 'CELL_D']:\n",
    "        data_out[col] = data[col]\n",
    "    for col in data.columns:\n",
    "        col = str(col)\n",
    "        # Lump all the healthy states into one.\n",
    "        if col.find('CELL_H') != -1 and not col == 'CELL_H':\n",
    "            data_out['CELL_H'] += data[col]\n",
    "\n",
    "    cleaned_sim_cell_counts[model] = data_out.copy()\n",
    "\n",
    "# Process all cell counts\n",
    "os.chdir(CELL_COUNT_DATA_DIR)\n",
    "cleaned_cell_counts = {}\n",
    "\n",
    "for model in models:\n",
    "    data = pd.read_csv(f'{model}_cell_counts.csv')\n",
    "    if model != 'moonchai':\n",
    "        data['CELL_N'] = [0] * len(data)\n",
    "    data['Model'] = [pretty_name[model]] * len(data)\n",
    "    data = proc_features(model, data.copy())\n",
    "    cleaned_cell_counts[model] = data.copy()\n",
    "\n",
    "# Merge\n",
    "os.chdir(SAVE_DIR)\n",
    "for model in models:\n",
    "    counts = cleaned_sim_cell_counts[model]\n",
    "    allcounts = cleaned_cell_counts[model]\n",
    "\n",
    "    allcounts.drop(['Model', 'L', 'Run'], axis=1, inplace=True)\n",
    "\n",
    "    counts.join(allcounts, how='outer').to_csv(f'{model}_cell_counts.csv', index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
