import numpy as np
import cv2
from numba import prange
import numpy as np
from skimage import feature as skif
from skimage.feature import blob_log

#TO DO: We still need the main method and file input/output to csv.

# Numerically encoded cell states, after being processed by
# the master script.
CELL_H = 1  # Healthy
CELL_A1 = 2  # Acute Infected
CELL_A2 = 3  # Latent Infected
CELL_D = 4  # Dead

# Self-defined color map. Used for openCV feature-extraction functions.
mycmap = {
    CELL_H: [0, 255, 0],
    CELL_A1: [255, 0, 0],
    CELL_A2: [0, 0, 255],
    CELL_D: [0, 0, 0],
}


# Helper function that takes a grid and increases the contrast
def adjust_array(array):
    max_val = np.max(array)
    return np.multiply(array, (255 / max_val))


# Helper function that takes a grid and returns an image operable by openCV.
def gridToOpenCVImage(grid):
    # Create a L by L by 3 np array
    shape = grid.shape
    res = np.zeros((shape[0], shape[1], 3), dtype=np.uint8)

    # Note that openCV stores image in BGR format. The colors
    # in mycmap are given in RGB.
    for row in range(len(grid)):
        for col in range(len(grid[0])):
            # Gets the color in mycmap and reverse it to BGR
            res[row][col] = mycmap[grid[row][col]][::-1]
    return res

# Helper function to run the SimpleBlobDetector algorithm on a given image.
# Return all found blobs with minimum area selected by input.
def getKeypoints(im, area):
    params = cv2.SimpleBlobDetector_Params()
    params.minThreshold = 1
    params.maxThreshold = 200
    params.filterByArea = True
    params.minArea = area
    params.filterByCircularity = False
    params.filterByConvexity = False
    params.filterByInertia = False
    detector = cv2.SimpleBlobDetector_create(params)
    return detector.detect(im)

#This function takes a grid as input and finds blob features. Returns a dictionary.
def getBlobFeatures(grid):
    # Helper function to compute the average distance among blobs
    def getAvgDistance(keypoints):
        if keypoints == []:
            return 0
        N = len(keypoints)
        res = 0
        for kpi in keypoints:
            for kpj in keypoints:
                x1, y1 = kpi.pt
                x2, y2 = kpj.pt
                res += np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
        return res / (2 * N)

    # Helper function to get the average size of the blobs
    def getAvgSize(keypoints):
        if keypoints == []:
            return 0
        return np.average([kp.size for kp in keypoints])

    # Extract image features and return them
    keypoints_5 = getKeypoints(gridToOpenCVImage(grid), 5)
    keypoints_10 = getKeypoints(gridToOpenCVImage(grid), 10)

    return {
        "5_num_blobs": len(keypoints_5),
        "5_avg_blob_dist": getAvgDistance(keypoints_5),
        "5_avg_blob_size": getAvgSize(keypoints_5),
        "10_num_blobs": len(keypoints_10),
        "10_avg_blob_dist": getAvgDistance(keypoints_10),
        "10_avg_blob_size": getAvgSize(keypoints_10)
    }

#This function takes a grid as input and finds the contour perimeter and area. Returns a dictionary.
def getContourFeatures(grid):

    # Grabs all contour edges from an image and calculates the perimeter of the contours
    def contour_perim(im, min_perim):
        imOrig = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY);
        ret, thresh = cv2.threshold(imOrig, 127, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[1:]  # First contour is the whole image, so it is removed

        cnt_perim = np.array(list(map(lambda x: cv2.arcLength(x, True), contours)))
        cnt_perim_filtered = cnt_perim[cnt_perim > min_perim]
        return cnt_perim_filtered if cnt_perim_filtered.size > 0 else [0]

    # Grabs all contour edges from an image and calculates the area of the contours
    def contour_area(im, min_area):
        imOrig = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY);
        ret, thresh = cv2.threshold(imOrig, 127, 255, 0)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[1:]  # First contour is the whole image, so it is removed

        cnt_area = np.array(list(map(lambda x: cv2.contourArea(x), contours)))
        cnt_area_filtered = cnt_area[cnt_area > min_area]
        return cnt_area_filtered if cnt_area_filtered.size > 0 else [0]

    im = getContourFeatures(grid)

    return {
        'cnt_area_10': len(contour_area(im, 10)), 'cnt_area_100': len(contour_area(im, 100)),
        'cnt_perim_10': len(contour_perim(im, 10)), 'cnt_perim_20': len(contour_perim(im, 20)),
        'avg_area_10': np.mean(contour_area(im, 10)), 'avg_area_100': np.mean(contour_area(im, 100)),
        'avg_perim_10': np.mean(contour_perim(im, 10)), 'avg_perim_20': np.mean(contour_perim(im, 20)),
        'max_cnt_area': max(contour_area(im, 0)), 'max_cnt_perim': max(contour_perim(im, 0))
    }

#This function takes a grid as input and gets the xvar and yvar. Returns a dictionary.
def getXvarYvar(grid):
    def getBlobs(data):
        img = data
        blobs_log = blob_log(img, min_sigma=10, max_sigma=30, num_sigma=10, threshold=0.01 / 255)
        return blobs_log
    
    def getBlobSize(blobs):
        return np.mean(blobs[:,2])
    
    def getVar_x(blobs):
        return np.var(blobs[:, 0]) / len(blobs)

    def getVar_y(blobs):
        return np.var(blobs[:, 1]) / len(blobs)

    keypoints = getBlobs(grid)
    return {'numofLogBlob':len(keypoints), 'AvgSizeofLogblob':getBlobSize(keypoints),'blob_varx': getVar_x(keypoints), 'blob_vary': getVar_y(keypoints)}


#This function takes a grid as input and gets the corners, and avg manhatten and euclidean distances.
#Returns a dictionary.
def getCornersManEuc(grid):
    def total_euclidean_distance(x_coords, y_coords, n):
        from math import sqrt
        sum = 0
        interactions = 0
        for i in prange(n):
            for j in prange(i + 1, n):
                sum += sqrt((x_coords[i] - x_coords[j]) ** 2 + (y_coords[i] - y_coords[j]) ** 2)
                interactions += 1

        return sum / interactions

    def total_manhatten_distance(x_coords, y_coords, n):
        sum = 0
        interactions = 0
        for i in prange(n):
            for j in prange(i + 1, n):
                sum += (abs(x_coords[i] - x_coords[j]) + abs(y_coords[i] - y_coords[j]))
                interactions += 1

        return sum / interactions

    def blob_features(array):
        blobs = skif.blob_doh(array)
        num_blobs = len(blobs)

        # collect the distances and size features
        x_coords = []
        y_coords = []
        for blob in blobs:
            x_coords.append(blob[1])
            y_coords.append(blob[0])

        avg_man_dist = total_manhatten_distance(x_coords, y_coords, num_blobs)
        avg_euc_dist = total_euclidean_distance(x_coords, y_coords, num_blobs)

        return avg_man_dist, avg_euc_dist

    def corner_features(array):
        # first, find potential corners, then find the most likely (or "peaks") of those likely corners
        corners = skif.corner_peaks(skif.corner_harris(array))

        num_corners = len(corners)

        # collect distance features
        x_coords = []
        y_coords = []
        for corner in corners:
            x_coords.append(corner[0])
            y_coords.append(corner[1])

        avg_man_dist = total_manhatten_distance(x_coords, y_coords, num_corners)
        avg_euc_dist = total_euclidean_distance(x_coords, y_coords, num_corners)

        return num_corners, avg_man_dist, avg_euc_dist

    features = {}
    image = adjust_array(grid)
    num_blobs, avg_radius, avg_man_dist_blobs, avg_euc_dist_blobs = blob_features(image)
    num_corners, avg_man_dist_corners, avg_euc_dist_corners = corner_features(image)

    # blob features in dict
    features["Avg Manhatten Dist Between Blobs"] = avg_man_dist_blobs
    features["Avg Euclidean Dist Between Blobs"] = avg_euc_dist_blobs

    # corner features in dict
    features["Num Corners"] = num_corners
    features["Avg Manhatten Dist Between Corners"] = avg_man_dist_corners
    features["Avg Euclidean Dist Between Corners"] = avg_euc_dist_corners
    return features

#Use each unique feature function to create a dictionary.
blobDict = getBlobFeatures(inputGrid)
contDict = getContourFeatures(inputGrid)
varDict = getXvarYvar(inputGrid)
corner_E_M_Dict = getCornersManEuc(inputGrid)
#Merge the dictionaries into one
featureDict = {**blobDict, **contDict, **varDict, **corner_E_M_Dict}

#TO DO: We still need to output the merged featureDict to a CSV file.