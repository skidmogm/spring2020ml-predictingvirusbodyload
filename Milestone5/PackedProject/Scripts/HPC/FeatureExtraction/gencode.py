doss_dict = {0: 1, 4: 2, 1: 3, 8: 4}
moonchai_dict = {0: 0, 4: 1, 2: 2, 1: 3, 8: 4}
precharattana_dict = {0: 1, 4: 2, 1: 2, 2: 3, 8: 4}
rana_dict = {0: 1, 2: 1, 8: 1, 10: 1, 4: 2, 1: 3, 16: 4}
goz_dict = {0: 1, 1: 1, 2: 1, 3: 1, 84: 2, 21: 3, 4: 4}

for dic in [doss_dict, moonchai_dict, precharattana_dict, rana_dict, goz_dict]:
    print('\t\t\t\tvalue = im[row][col]'.replace('\t', '    '))
    for ind, k in zip(range(len(dic)), dic.keys()):
        if ind == 0:
            print('\t\t\t\tif '.replace('\t', '    '), end='')
        else:
            print('\t\t\t\telif '.replace('\t', '    '), end='')

        print('value == {}:'.format(k))
        print('\t\t\t\t\tim_new[row][col] = {}'.format(dic[k]).replace('\t', '    '))

    print('')
