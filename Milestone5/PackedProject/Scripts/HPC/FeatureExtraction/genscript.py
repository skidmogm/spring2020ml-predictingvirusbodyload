for model in ['rana', 'dossantos', 'gonzalez', 'precharattana', 'moonchai', 'gonzalez']:
    for L in ['800', '1000', '1200']:
        fname = '{}{}.py'.format(model, L)
        with open('script.py') as fin, open(fname, 'w') as fout:
            for line in fin:
                if line.strip() == "models = ['dossantos', 'moonchai', 'precharattana', 'rana', 'gonzalez']":
                    fout.write('models = [\'{}\']\n'.format(model))
                elif line.strip() == 'Ls = [800, 1000, 1200]':
                    fout.write('Ls = [{}]\n'.format(L))
                elif line.strip() == 'f = open(model + "_grid_features.csv", \'w\')':
                    # We indented by spaces
                    fout.write('        f = open(model + "_{}_grid_features.csv", "w")\n'.format(L))
                else:
                    fout.write(line)
