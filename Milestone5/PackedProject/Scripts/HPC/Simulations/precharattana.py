#!/usr/bin/env python
# coding: utf-8

# Investigation of Spatial Pattern Formation Involving CD4+ T Cells in HIV/AIDS Dynamics by a Stochastic Cellular Automata Model

import numpy as np
from numba import jit, prange
import os

# Constants
CELL_H = 0
CELL_A2 = 1
CELL_A0 = 2
CELL_A1 = 4
CELL_D = 8


@jit(nopython=True, parallel=True)
def step(grid, buffer, taugrid, P_inf, P_act, P_repl, tau1, tau2):
    for i in prange(1, grid.shape[0] - 1):
        for j in range(1, grid.shape[1] - 1):
            cell = grid[i, j]

            # (1) Rule for Healthy cells
            if cell == CELL_H:
                # If a healthy cell is in contact with at least one infected cell stage 1 (A1)
                # or at least R cells of infected cell stage 2 (A2),
                # (A) The healthy cell becomes an infected cell stage 1 (A1) with the probability P_inf.
                # (B) The healthy cell becomes a latently infected cell (A0) with the probability 1 - P_inf.
                neighbor_sum = ((grid[i - 1, j - 1] & 5) + (grid[i - 1, j] & 5) + (grid[i - 1, j + 1] & 5) +
                                (grid[i, j - 1] & 5) + (grid[i, j + 1] & 5) +
                                (grid[i + 1, j - 1] & 5) + (grid[i + 1, j] & 5) + (grid[i + 1, j + 1] & 5))
                if neighbor_sum >= 4:
                    if np.random.random() < P_inf:
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_A0

            # (2) Rule for infected cells stage 1
            elif cell == CELL_A1:
                # If an A1 cell has lived in the system for longer than tau1 time steps,
                # the A1 cell becomes an A2 cell. Otherwise, it remains the same state. 
                taugrid[i, j] += 1
                if taugrid[i, j] >= tau1:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            # (3) Rule for infected cells stage 2
            elif cell == CELL_A2:
                # An A2 cell becomes a dead cell (D) at the following step.
                buffer[i, j] = CELL_D

            # (4) Rule for dead cells
            elif cell == CELL_D:
                # A dead cell (D) is replaced by a healthy cell with the probability P_repl.
                # Otherwise it remains unchanged with the probability 1 - P_repl. 
                if np.random.random() < P_repl:
                    buffer[i, j] = CELL_H

            # (5) Rule for latently infected cells
            else:
                # If an A0 cell has lived in the system for longer than tau2 time steps,
                # the A0 cell becomes an A1 cell with the probability P_act. Otherwise, it stays unchanged.
                if taugrid[i, j] < tau2:  # Avoid overflow
                    taugrid[i, j] += 1
                if taugrid[i, j] >= tau2:
                    if np.random.random() < P_act:
                        taugrid[i, j] = 0
                        buffer[i, j] = CELL_A1


@jit
def count(grid, w=1):
    healthy = 0
    dead = 0
    infected = 0
    latent = 0
    for i in range(w, grid.shape[0] - w):
        for j in range(w, grid.shape[1] - w):
            cell = grid[i, j]
            if cell == CELL_H:
                healthy += 1
            elif cell == CELL_D:
                dead += 1
            elif cell == CELL_A0:
                latent += 1
            else:
                infected += 1
    return healthy, infected, latent, dead


@jit
def init_grid(L, P_HIV, w=1):
    grid = np.zeros((L + w * 2, L + w * 2), dtype=np.uint8)

    for i in range(w, grid.shape[0] - w):
        for j in range(w, grid.shape[1] - w):
            if np.random.random() < P_HIV:
                grid[i, j] = CELL_A1

    return grid, np.zeros_like(grid)


def periodic_borders(grid, w=1):
    grid[:, :w] = grid[:, -w - w:-w]
    grid[:, -w:] = grid[:, w:w + w]
    grid[:w, :] = grid[-w - w:-w, :]
    grid[-w:, :] = grid[w:w + w, :]


def run(file_name, timesteps, replications, L=100, P_HIV=0.005, P_inf=0.999, P_act=0.0025, P_repl=0.99, tau1=4,
        tau2=30):
    """
    Runs the model.
    """

    def save_array(grid, run, t):
        # will save under the file structure model_name/length/simulation_#/timestep
        file_name = "{}.npz".format(t)
        np.savez_compressed(file_name, arr_0=grid)

    file = open(file_name, 'a')

    for run in range(replications):
        grid, taugrid = init_grid(L, P_HIV)
        buffer = grid.copy()

        # Depending on the run, change to correct directory to save files
        SAVE_DIR = os.path.join(MODEL_ROOT_DIR, str(L), str(run + 1))
        if not os.path.exists(SAVE_DIR):
            os.makedirs(SAVE_DIR)

        os.chdir(SAVE_DIR)

        # Save the t=0 grid
        save_array(grid, run, 0)

        for t in range(timesteps):
            periodic_borders(grid)
            healthy, infected, latent, dead = count(grid)

            # Save cell counts
            if t == timesteps - 1:
                # ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p',
                # 'CELL_H_rtp', 'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']
                data = ['Gonzalez', str(run + 1), str(L), str(healthy), '', '', '', '', '', '', '',
                        str(dead), str(latent), str(infected), '']
                file.write(','.join(data) + '\n')
                file.flush()

            step(grid, buffer, taugrid, P_inf, P_act, P_repl, tau1, tau2)
            grid[:, :] = buffer[:, :]

            save_array(grid, run, t + 1)


COLUMNS = ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp',
           'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']

# From where you start the script
ROOT_DIR = os.getcwd()
MODEL_ROOT_DIR = os.path.join(ROOT_DIR, 'precharattana')


def main(grid_sizes, replications):
    Ls = grid_sizes

    p_hivs = [0.005]
    p_repls = [0.99]
    p_infecs = [0.999]

    tauls = [4]
    replications = replications
    timesteps = 600

    # Check if the root directory for the model exists
    # If not, create a new directory
    if not os.path.exists(MODEL_ROOT_DIR):
        os.mkdir(MODEL_ROOT_DIR)

    os.chdir(MODEL_ROOT_DIR)

    # Run and output the simulations
    for L in Ls:
        for p_hiv in p_hivs:
            for p_repl in p_repls:
                for p_infec in p_infecs:
                    for taul in tauls:
                        SAVE_DIR = os.path.join(MODEL_ROOT_DIR, str(L))
                        if not os.path.exists(SAVE_DIR):
                            os.mkdir(SAVE_DIR)
                        os.chdir(SAVE_DIR)
                        file_name = os.path.join(SAVE_DIR, "precharattana_" + str(L) + ".csv")

                        # Create the output file when the file is not there so that
                        # we do not write the header multiple times
                        if not os.path.exists(file_name):
                            f = open(file_name, 'a')
                            f.write(','.join(COLUMNS) + '\n')
                            f.close()
                        # Run the simulation
                        os.chdir(MODEL_ROOT_DIR)
                        run(file_name, timesteps, replications, L, p_hiv, p_infec, 0.025, p_repl, taul)
