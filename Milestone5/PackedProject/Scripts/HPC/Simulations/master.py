###################################################
# This file allows you to run HIV simulations
#
# Author: Andrew Fisher
###################################################

import argparse

import dos_santos
import gonzalez
import moonchai
import precharattana
import rana


def main(args):
    simulation = args.simulation
    grid_sizes = args.grid_sizes
    replications = int(args.replications)

    # Convert parameters to ints
    for i in range(len(grid_sizes)):
        grid_sizes[i] = int(grid_sizes[i])

    if (simulation == 'dossantos'):
        dos_santos.main(grid_sizes, replications)
    elif (simulation == 'gonzalez'):
        gonzalez.main(grid_sizes, replications)
    elif (simulation == 'moonchai'):
        moonchai.main(grid_sizes, replications)
    elif (simulation == 'precharattana'):
        precharattana.main(grid_sizes, replications)
    elif (simulation == 'rana'):
        rana.main(grid_sizes, replications)
    else:
        print(
            "Simulation '" + simulation + "' not recognized. Please pass one of the following: dossantos, gonzalez, moonchai, precharattana, or rana")
        exit(0)


if __name__ == '__main__':
    # Create the argument parser
    ap = argparse.ArgumentParser(description="The master script for running batch jobs of HIV simulations")

    # Add the arguments
    ap.add_argument("-s", required=True,
                    help="The simulation to run: dossantos, gonzalez, moonchai, precharattana, or rana",
                    type=str, dest="simulation")

    ap.add_argument("-g", action='append',
                    help="The grid size(s) to use (pass this argument multiple times for multiple grid sizes)",
                    required=True, dest="grid_sizes")

    ap.add_argument("-r", required=True, help="The number of replications to do for each grid size",
                    type=str, dest="replications")

    # Parse the arguments
    args = ap.parse_args()

    # Launch the simulation
    main(args)
