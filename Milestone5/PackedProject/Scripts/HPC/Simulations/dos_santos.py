# The Dos Santos Model

import numpy as np
import os
from numba import jit, prange

# Constants for the model
CELL_H = 0
CELL_D = 8
CELL_A1 = 4
CELL_A2 = 1

COLUMNS = ['Model', 'Run', 'L', 'CELL_H', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp', 'CELL_N',
           'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']

# From where you start the script
ROOT_DIR = os.getcwd()
MODEL_ROOT_DIR = os.path.join(ROOT_DIR, 'dossantos')


def main(grid_sizes, replications):
    # Check if the root directory for the model exists
    # If not, create a new directory
    if not os.path.exists(MODEL_ROOT_DIR):
        os.mkdir(MODEL_ROOT_DIR)

    # Run and output the simulations
    for L in grid_sizes:
        SIZE_DIR = os.path.join(MODEL_ROOT_DIR, str(L))
        if not os.path.exists(SIZE_DIR):
            os.mkdir(SIZE_DIR)
        os.chdir(SIZE_DIR)
        file_name = os.path.join(SIZE_DIR, "dossantosout_" + str(L) + ".csv")

        # Create the output file when the file is not there so that
        # we do not write the header multiple times
        if not os.path.exists(file_name):
            f = open(file_name, 'a')
            f.write(','.join(COLUMNS) + '\n')
            f.close()

        os.chdir(MODEL_ROOT_DIR)
        run(file_name, L, replications=replications)


@jit(nopython=True, parallel=True)
def simulation_step(grid, buffer, taugrid, P_repl, P_infec, tau1):
    healthy = 0
    dead = 0
    infected_a1 = 0
    infected_a2 = 0

    for i in prange(1, grid.shape[0] - 1):
        for j in prange(1, grid.shape[1] - 1):
            cell = grid[i, j]

            if cell == CELL_H:
                healthy += 1

                if ((grid[i - 1, j - 1] & 5) + (grid[i - 1, j] & 5) + (grid[i - 1, j + 1] & 5) +
                    (grid[i, j - 1] & 5) + (grid[i, j + 1] & 5) +
                    (grid[i + 1, j - 1] & 5) + (grid[i + 1, j] & 5) + (grid[i + 1, j + 1] & 5)) >= 4:
                    buffer[i, j] = CELL_A1

            elif cell == CELL_A1:
                infected_a1 += 1
                taugrid[i, j] += 1
                if taugrid[i, j] == tau1:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif cell == CELL_A2:
                infected_a2 += 1
                buffer[i, j] = CELL_D

            else:  # elif value == CELL_D:
                dead += 1

                if np.random.random() < P_repl:
                    if np.random.random() < P_infec:
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_H

    return healthy, infected_a1, infected_a2, dead


def run(file_name, L, timesteps=600, replications=300, P_HIV=0.05, P_repl=0.99, P_infec=1e-05, tau1=4):
    """
    Runs the model.
    """

    @jit
    def init_grid():
        grid = np.zeros((L + 2, L + 2), dtype=np.uint8)

        # seeding the virus
        for i in prange(1, grid.shape[0] - 1):
            for j in prange(1, grid.shape[1] - 1):
                if np.random.random() < P_HIV:
                    grid[i, j] = CELL_A1

        return grid, np.zeros_like(grid)

    def periodic_borders(grid):
        grid[1:-1, 0] = grid[1:-1, -2]
        grid[1:-1, -1] = grid[1:-1, 1]
        grid[0, 1:-1] = grid[-2, 1:-1]
        grid[-1, 1:-1] = grid[1, 1:-1]
        grid[0, 0] = grid[-2, -2]
        grid[0, -1] = grid[-2, 1]
        grid[-1, 0] = grid[1, -2]
        grid[-1, -1] = grid[1, 1]

    def save_array(grid, run, t):
        # will save under the file structure model_name/length/simulation_#/timestep
        file_name = "{}.npz".format(t)
        np.savez_compressed(file_name, arr_0=grid)

    file = open(file_name, 'a')

    for run in range(replications):
        grid, taugrid = init_grid()
        buffer = grid.copy()

        # Depending on the run, change to correct directory to save files
        SAVE_DIR = os.path.join(MODEL_ROOT_DIR, str(L), str(run + 1))
        if not os.path.exists(SAVE_DIR):
            os.makedirs(SAVE_DIR)

        os.chdir(SAVE_DIR)

        # Save the t=0 grid
        save_array(grid, run, 0)

        for t in range(timesteps):
            periodic_borders(grid)
            healthy, infected_a1, infected_a2, dead = simulation_step(grid, buffer, taugrid, P_repl, P_infec, tau1)

            # Save cell counts
            if t == timesteps - 1:
                # ['Model', 'Run', 'L', 'CELL_H', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp', 'CELL_N',
                #            'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']
                data = ['Dos Santos', str(run + 1), str(L), str(healthy), '', '', '', '',
                        str(dead), '', str(infected_a1), str(infected_a2)]
                file.write(','.join(data) + '\n')
                file.flush()

            # We have propagated in time once ALREADY, this will be t+1.
            grid[:, :] = buffer[:, :]
            save_array(grid, run, t + 1)

    file.close()


def plot_pic(file_name, grid_size):
    import numpy as np
    import scipy.misc as smp
    import matplotlib.pyplot as plt

    picture = np.load(file_name)['arr_0']

    fig = plt.figure(figsize=(8, 15))
    columns = 3
    rows = 2

    # create a grid_size x grid_size x 3 numpy array of 0's
    data = np.zeros((grid_size, grid_size, 3), dtype=np.uint8)
    # Convert picture array to image
    for row in range(0, len(data)):
        for col in range(0, len(data)):
            pixel = picture[row + 1][col + 1]
            if pixel == 0:
                data[row, col] = [255, 255, 255]  # CELL_H  == WHITE
            elif (pixel == 8):
                data[row, col] = [0, 0, 0]  # CELL_D  == BLACK
            elif (pixel == 4):
                data[row, col] = [0, 128, 0]  # CELL_A1 == GREEN
            elif (pixel == 1):
                data[row, col] = [0, 255, 0]  # CELL_A2 == LIME

    tmp = fig.add_subplot(rows, columns, 1)
    plt.imshow(data)

    plt.show()
