#!/usr/bin/env python
# coding: utf-8

# Moonchai and Lenbury (2014)
import os
import numpy as np
import math
from numba import jit, prange

# Cell states
# Note: default state should have the value of 0
CELL_H = 4  # Healthy cells are CD4+ T-cells which are the main target of HIV
CELL_N = 0  # Non-activated cells are cells which may become an active healthy cell
CELL_D = 8  # Dead cells are infected cells killed by the immune response
CELL_A1 = 2  # Infected A1 cells are infected cells in the earlier stages which are free to spread the infection.
CELL_A2 = 1  # Infected A2 cells are infected cells in the final stage before dying due to the action of the immune system.


@jit(nopython=True, parallel=True)
def step(grid, buffer, taugrid, P_v, a, V_t, P_d, r1, r2, P_op, P_repl, R, tau, P_nona):
    P_vStar = P_v * (1 - math.exp(-a * V_t))
    P_1Star = r1 * (1 - P_vStar)
    P_2Star = r2 * (1 - r1) * (1 - P_vStar)

    for i in prange(1, grid.shape[0] - 1):  # prange for parallelization
        for j in range(1, grid.shape[1] - 1):
            cell = grid[i, j]

            if cell == CELL_N:
                neighbors = (grid[i - 1, j - 1] | grid[i - 1, j] | grid[i - 1, j + 1] |
                             grid[i, j - 1] | grid[i, j + 1] |
                             grid[i + 1, j - 1] | grid[i + 1, j] | grid[i + 1, j + 1])

                # a) If a non-proliferating cell has only non-proliferating cells as its neighbors, it may become an active healthy cell at the
                #    probability P_op, accounting for opportunistic infection, or it remains the same at the probability 1-P_op.
                if neighbors == CELL_N:
                    if np.random.random() < P_op:
                        buffer[i, j] = CELL_H
                        continue

                # b) If it has a neighbor which is A1 or A2 infected, it becomes an active healthy cell, by which the body tries to fight the infection.
                elif neighbors & (CELL_A1 + CELL_A2):
                    buffer[i, j] = CELL_H
                    continue

                # c) After steps (a) and (b) a non-proliferating cell can be replaced by an infected A1 cell with the probability P_repl. Otherwise, it
                #    remains a non-proliferating cell at the probability 1-P_repl.
                if np.random.random() < P_repl:
                    buffer[i, j] = CELL_A1

            elif cell == CELL_H:
                # a) An active healthy cell gets infected by coming in contact with a virus at the probability P_v* = P_v(1-e^(-aV_t)), where V_t
                #    represents the viral load in that compartment.
                if np.random.random() < P_vStar:
                    buffer[i, j] = CELL_A1
                else:
                    # b) If it has at least one infected A1 - neighbor, it becomes an infected A1 cell at the probability P_1Star = r1*(1-P_vStar)
                    if ((grid[i - 1, j - 1] & CELL_A1) + (grid[i - 1, j] & CELL_A1) + (grid[i - 1, j + 1] & CELL_A1) +
                        (grid[i, j - 1] & CELL_A1) + (grid[i, j + 1] & CELL_A1) +
                        (grid[i + 1, j - 1] & CELL_A1) + (grid[i + 1, j] & CELL_A1) + (
                                grid[i + 1, j + 1] & CELL_A1)) != 0:
                        if np.random.random() < P_1Star:
                            buffer[i, j] = CELL_A1
                    else:
                        # c) If it has no infected A1 neighbor, but has at least R (2<R<8) infected A2 neighbors, it becomes an infected A1 cell at the
                        #    probability P_2Star = r2*(1-r1)*(1-P_vStar)
                        if ((grid[i - 1, j - 1] & CELL_A2) + (grid[i - 1, j] & CELL_A2) + (
                                grid[i - 1, j + 1] & CELL_A2) +
                            (grid[i, j - 1] & CELL_A2) + (grid[i, j + 1] & CELL_A2) +
                            (grid[i + 1, j - 1] & CELL_A2) + (grid[i + 1, j] & CELL_A2) + (
                                    grid[i + 1, j + 1] & CELL_A2)) >= R * CELL_A2:
                            if np.random.random() < P_2Star:
                                buffer[i, j] = CELL_A1

                # d) Otherwise, it remains a healthy cell at the probability 1 - P_1Star - P_2Star - P_vStar, where 0 < 1 - P_1Star - P_2Star - P_vStar < 1

                # After step (d), an infected A1 cell remains an infected A1 cell at the probability 1 - P_d(t).
                if np.random.random() < P_d:
                    buffer[i, j] = CELL_H

            elif cell == CELL_A1:
                # An infected A1 cell becomes an infected A2 cell after tau time steps. Thus, different infected A1 cells become infected A2 cells at
                # different times with a delay of tau.
                taugrid[i, j] += 1
                if taugrid[i, j] == tau:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif cell == CELL_A2:
                # Infected A2 cells become dead cells, corresponding to the depletion of infected cells by the immune response
                buffer[i, j] = CELL_D

            elif cell == CELL_D:
                # A dead cell can be replaced by an inactivated cell with the probability P_nona. Otherwise, it remains a dead cell at the probability
                # 1-P_nona.
                if np.random.random() < P_nona:
                    buffer[i, j] = CELL_N


# Returns the grid and the taugrid
@jit
def init_grid(L, P_HIV, H_0):
    # Periodic boundary condition
    # I create the grid with an extra border to act as a virtual border. Then before each step, I copy the real borders onto the appropriate virtual
    # border to create the periodicity.
    grid = np.zeros((L + 2, L + 2), dtype=np.uint8)

    grid_real = grid[1:-1, 1:-1]

    # There is a fixed number of H cells, and A1 cells at start
    # grid[1:-1,1:-1].flat[0:H_0] = CELL_H
    # grid[1:-1,1:-1].flat[H_0:H_0+int(P_HIV*H_0)] = CELL_A1
    # np.random.shuffle(grid[1:-1,1:-1].flat)

    i = H_0
    while i > 0:
        r = np.random.randint(grid_real.size)
        if grid_real.flat[r] == 0:
            grid_real.flat[r] = CELL_H
            i -= 1

    i = int(P_HIV * H_0)
    while i > 0:
        r = np.random.randint(grid_real.size)
        if grid_real.flat[r] == 0:
            grid_real.flat[r] = CELL_A1
            i -= 1

    return grid, np.zeros_like(grid)


def periodic_borders(grid):
    grid[1:-1, 0] = grid[1:-1, -2]
    grid[1:-1, -1] = grid[1:-1, 1]
    grid[0, 1:-1] = grid[-2, 1:-1]
    grid[-1, 1:-1] = grid[1, 1:-1]
    # grid[ 0, 0] = grid[-2,-2]
    # grid[ 0,-1] = grid[-2, 1]
    # grid[-1, 0] = grid[ 1,-2]
    # grid[-1,-1] = grid[ 1, 1]


def run(file_name, T=600, RUNS=50, L=500, N_0=250_000, H_0=125000, P_HIV=0.05, P_op=0.001, r1=0.997, r2=0.997, mu=0.9,
        eta=0.05,
        tau=4, P_repl=1e-16, P_nona=0.9, R=4, p=480, S_L=2e11 / 125_000, S_B=1000 / 125_000, C_LH=4e-6, C_BH=4e-6,
        c=0.3, e=0.02, beta=4e-9, alpha=2.5e8, eps=0):
    file = open(file_name, 'a')

    for run in range(RUNS):
        grid_lymph, taugrid_lymph = init_grid(L, P_HIV, H_0)
        grid_blood, taugrid_blood = init_grid(L, P_HIV, H_0)

        buffer_lymph = grid_lymph.copy()
        buffer_blood = grid_blood.copy()

        V_tB = 10
        V_tL = 0

        def save_array(grid, id, t):
            # will save under the file structure model_name/length/simulation_#/timestep
            file_name = "{}{}.npz".format(t, id)
            np.savez_compressed(file_name, arr_0=grid)

        # Depending on the run, change to correct directory to save files
        SAVE_DIR = os.path.join(MODEL_ROOT_DIR, str(L), str(run + 1))
        if not os.path.exists(SAVE_DIR):
            os.makedirs(SAVE_DIR)

        os.chdir(SAVE_DIR)

        # Save the t=0 grid
        save_array(grid_lymph, '', 0)
        # save_array(grid_blood, 'b', 0)

        for t in range(T):
            # Resolve cell counts
            A_1tL = np.count_nonzero(grid_lymph[1:-1, 1:-1] == CELL_A1)
            A_2tL = np.count_nonzero(grid_lymph[1:-1, 1:-1] == CELL_A2)
            H_tL = np.count_nonzero(grid_lymph[1:-1, 1:-1] == CELL_H)
            A_1tB = np.count_nonzero(grid_blood[1:-1, 1:-1] == CELL_A1)
            A_2tB = np.count_nonzero(grid_blood[1:-1, 1:-1] == CELL_A2)
            H_tB = np.count_nonzero(grid_blood[1:-1, 1:-1] == CELL_H)

            # Update the cell states in the lymph lattice
            step(grid_lymph, buffer_lymph, taugrid_lymph, P_v=0.0002, a=1e-16, V_t=V_tL, P_d=0, r1=r1, r2=r2, P_op=P_op,
                 P_repl=P_repl, R=R, tau=tau, P_nona=P_nona)
            np.copyto(grid_lymph, buffer_lymph)

            # Update the cell states in the blood lattice
            step(grid_blood, buffer_blood, taugrid_blood, P_v=0.0001, a=1e-7, V_t=V_tB, P_d=0, r1=r1, r2=r2, P_op=P_op,
                 P_repl=P_repl, R=R, tau=tau, P_nona=P_nona)
            np.copyto(grid_blood, buffer_blood)

            # Update viral load model
            I_tL = A_1tL + A_2tL
            I_tB = A_1tB + A_2tB

            V_L = e * (V_tL + alpha * V_tB)
            V_B = e * (beta * V_tL + V_tB)

            V_tL = p * S_L * I_tL + (alpha * V_tB - V_L) - C_LH * H_tL * V_tL - c * V_tL + V_tL
            V_tB = p * S_B * I_tB + (V_B - V_tB) - C_BH * H_tB * V_tB - c * V_tB - eps * V_tB + V_tB

            # Save cell counts
            if t == T - 1:
                # ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp',
                # 'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2', 'BLOOD_H', 'BLOOD_D', 'BLOOD_A1', 'BLOOD_A2']
                data = ['Moonchai', str(run + 1), str(L), str(H_tL), '', '', '', '', '', '', '',
                        str(L * L - H_tL - A_1tL - A_2tL), '', str(A_1tL), str(A_2tL),
                        str(H_tB), str(L * L - H_tB - A_1tB - A_2tB), str(A_1tB), str(A_2tB)]
                file.write(','.join(data) + '\n')

            # We have propagated in time once ALREADY, this will be t+1.
            save_array(grid_lymph, '', t + 1)
            # save_array(grid_blood, 'b', t + 1)


COLUMNS = ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp',
           'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2', 'BLOOD_H', 'BLOOD_D', 'BLOOD_A1', 'BLOOD_A2']

# From where you start the script
ROOT_DIR = os.getcwd()
MODEL_ROOT_DIR = os.path.join(ROOT_DIR, 'moonchai')


def main(grid_sizes, replications):
    Ls = grid_sizes

    p_hivs = [0.05]
    p_repls = [1e-16]
    p_infecs = [0.001]

    tauls = [4]
    replications = replications
    timesteps = 600

    # Check if the root directory for the model exists
    # If not, create a new directory
    if not os.path.exists(MODEL_ROOT_DIR):
        os.mkdir(MODEL_ROOT_DIR)

    os.chdir(MODEL_ROOT_DIR)
    file_name = os.path.join(MODEL_ROOT_DIR, "moonchaiout.csv")

    # Create the output file when the file is not there so that
    # we do not write the header multiple times
    if not os.path.exists(file_name):
        f = open(file_name, 'a')
        f.write(','.join(COLUMNS) + '\n')
        f.close()

    # Run and output the simulations
    run_count = 0
    for L in Ls:
        for p_hiv in p_hivs:
            for p_repl in p_repls:
                for p_infec in p_infecs:
                    for taul in tauls:
                        run(file_name, timesteps, replications, L, L * L, (L * L) / 2, p_hiv, p_infec,
                            0.997, 0.997, 0.9, 0.05, taul, p_repl)
