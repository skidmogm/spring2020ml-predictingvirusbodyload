# Description of Directory
Some of these descriptions are also given in the notebooks responsible for processing these data.

## Simulation Data and Feature Extraction Data Conventions
As always, we have 5 models, usually denoted by `rana`, `dossantos`, `gonzalez`, `precharattana`, and `moonchai`. We ran each simulation model 300 times for grid sizes $800\times800$, $1000\times1000$, and $1200\times1200$.

### Simulation Cell Counts (`sim_cell_count`) *at the END of Each Run* 
While running each model, we also recorded the count of each cell type at the very last time step of the simulation, and compiled them to a csv file. These simulation cell counts are given in the following files --
* `ranaout.csv`
* `dossantosout_800.csv`, `dossantosout_1000.csv`, `dossantosout_1200.csv`
* `gonzalezout.csv`
* `moonchaiout.csv`
* `precharattana_800.csv`, `precharattana_1000.csv`, `precharattana_1200.csv`

For some of the models, data are split further by grid size since we ran the scripts in parallel.

### Extracted Features (`features`)
While running each model, we also saved the grid at each time step. We later then ran a feature extraction script over these saved scripts to get a csv of image data. Some fields are missing because they are discarded due to unreasonable running times (5 mins for one "cell" in the csv, no thanks). The raw output of these scripts are in the following file format -- `[model name]_[grid size]_grid_features.csv`, for example, `rana_800_grid_features.csv`.

### All Cell Counts (`all_cell_count`) *at Every 5 Time Steps of the Simulation*
To compare the effectiveness of image features, we also extracted the cell count at each time step of the simulation in intervals of 5, and treat these cell counts as our "image feature". The structure will be similar to that of image features.

## EarlyTreatment
This directory contails all the same data as described above, but for running the models with treatment (rana and gonzalez) early, separately at weeks 2, 4, and 8. Since they have the same name and are nested one level deeper, they are placed in this separate directory to avoid confusion. 
