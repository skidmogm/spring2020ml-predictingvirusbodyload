\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{style}

\LoadClass[twocolumn]{article}
\usepackage[utf8]{inputenc}
\usepackage{physics}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage[top=1in,bottom=1in,left=0.7in,right=0.7in]{geometry}
\usepackage{lipsum}
% \usepackage{geometry}
\usepackage{enumitem}
\usepackage{mathrsfs}
\usepackage{bbm}
\usepackage{mathtools}
\usepackage{xcolor}
% \usepackage{textcomp}
\usepackage{minted}
\usepackage{longtable,ltcaption}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}

\usepackage{multirow}
\usepackage{booktabs}
\usepackage{caption}

\newenvironment{solution}
  {\begin{proof}[Solution]}
  {\end{proof}}
 
% Math commands
\newcommand{\nats}{\mathbb{N}}
\newcommand{\reals}{\mathbb{R}}
\newcommand{\ints}{\mathbb{Z}}
\newcommand{\forward}{\implies}
\newcommand{\backward}{\Longleftarrow}

% Physics commands
\newcommand{\avg}[1]{\langle#1\rangle}
\newcommand{\oper}[1]{\hat{#1}}
\newcommand{\pp}[1]{\phantom{#1}}
\newcommand{\E}{\mathscr{E}}
\newcommand{\lap}{\nabla^2}
\newcommand{\adag}{\oper a^\dag}
\newcommand{\opera}{\oper a}
\let\olddd\dd
\renewcommand{\dd}{\,\olddd}

\renewcommand{\theFancyVerbLine}{\small{\ttfamily{\arabic{FancyVerbLine}}}}
