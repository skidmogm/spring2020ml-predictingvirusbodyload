\section{Methods}
For this research, \textit{cellular automata} (CA) reproduced the effects of Human Immunodeficiency Virus (HIV) within the human body. There are five implementations of HIV modeling used: the dos Santos, Gonz\'alez, Moonchai, Precharattana, and Rana models, which will be further discussed in section 2.1. From several thousand simulations of these models at various sizes (side lengths of $800\times 800$, $1000\times 1000$ and $1200\times 1200$), image features were collected every five timesteps until $t = 200$. These image features are described in detail in section 2.2. Once this step of data collection was complete, several regression models were used to predict the prevalence of each cell type at the end of the simulation for each of the HIV models; these regression models included the \textit{Decision Tree Regressor} (Tree), \textit{Gaussian Processes Regressor} (GPR), \textit{Lasso Regressor} (Lasso), and \textit{Support Vector Regressor} (SVR). The regression models and \textit{hyperparameter tuning} (HT) are described in section 2.3.

\subsection{HIV Modeling using CA}
Researchers have worked on developing virtual laboratories using CA for about 30 years, dating back to the model of Kougias and Schulte in 1990 \cite{1990paper} and a new spur of research activity following the work of dos Santos and Coutinho in 2001 \cite{4}. Many such models are discussed in the 2016 review of CA for HIV by Precharattana \cite{36}, and some are briefly presented in a newer survey addressing the broader context of applied CA research \cite{2}. Noteworthy examples include a 2010 model by Precharattana \cite{37} which introduced the notion of ``reservoirs'' (i.e. cells in which HIV can hide for years), a 2013 model by Gonz\'alez, et. al, which accounted for two classes of drugs \cite{15}, a model by Rana, et al., presented at the ACM SIGSIM PADS'15 on the effect of lapse in treatment \cite{38}, a 2016 model by Moonchai and Lenbury experimenting on the effects of giving drugs and replacing a patient's blood plasma \cite{30}, and an ACM SIGSIM PADS'19 piece seeking to model two modes of HIV transmission between cells \cite{11}. 

Each of these HIV models use a form of CA. A cellular automaton is a simulation model composed of large numbers of simple components (called cells) which only interact with a small set of other components. These local interactions are defined as a neighborhood. In most HIV models apart from \cite{29}, cells are packed within a regular 2D lattice, that is, a grid. Each cell of the cellular automata simulation model represents a CD4+ T immune system cell in the human body. Within this grid, the neighborhood of a cell consists of the four cells that touch via a side (Von Neumann neighborhood) or also includes the other four cells that touch via a corner (Moore neighborhood). HIV CA models generally use a Moore neighborhood, although recent investigations suggested that neighborhoods of more than eight cells may be needed to capture different ways in which the HIV virion spreads \cite{11}. Although cells are packed in a grid, the boundaries (e.g., first row, last column) are a special case because cells at such locations have fewer neighbors, which can introduce unwanted irregularities into a simulation. All HIV CA apart from \cite{38} dealt with this special case by wrapping boundaries (figure \ref{fig: wrap around}) from one side onto the other (e.g., the top-left cell now also connects with the top-right cell and the bottom-left cell).\footnote{It should be noted that CA grids can be of different shapes such as a triangle or hexagon, and the cells can also be of different sizes. CAs are usually taught as a 2-D shape but they can be reformatted to the $n$th dimension such as 1-D or 3-D.}\input{images/wrap_around}

Each cell within the grid is given a state (healthy, infected, dead, or some other intermediate or special states, such as acute infected, healthy with treatment present, etc.) based on a particular ruleset of the simulation model. This process of kickstarting a CA simulation is called seeding, as the following steps of the model require a starting point to begin simulations. The model then uses the aforementioned ruleset to update each cell synchronously over discrete time steps based on its state and the states of cells within its neighborhood. ``Discrete'' in this context means that the entire grid is updated at one time for each time step. The time step itself represents the passage of a period of real-life time, which is weeks in the case of these HIV simulation models. Each model ends at $600$ timesteps, which is $600$ weeks of HIV simulation. An example of discrete time stepping is shown in figure \ref{fig: ca}, where 2-D cellular automaton evolved 3 times. The rule set of this CA is given below the time-evolution.
\input{images/example_update_rule}

Finally, it must be noted that there were modifications to the CA HIV models used by this team for the purposes of increasing performance. Based on previous findings \cite{reading2}, each model was modified to make use of Just In Time (JIT) compilation, parallel processing, Xoroshiro128+ Pseudo-Random Number Generation (PRNG), and neighborhood addition, which treats each state as a numerical value rather than a discrete class. These modifications allow for far faster processing of each full simulation run, allowing for more data to be collected in a shorter period of time.

\subsection{Image Feature Collection}
After running each HIV simulation model $300$ times at the three desired grid lengths, all of the timestep data was collected for image feature analysis. Each grid for each timestep can be viewed itself as a greyscale image. Using the SciKit-Image and OpenCV libraries, image features can be recognized in these greyscale images of each timestep. Once these features were recognized and labeled by the image libraries, a numerical value must be pulled from them to regress on. This poses a challenge: \textit{how does one turn the location of a feature into a numerical value?} For the purposes of this paper, this team focused on easily obtainable numerical values such as the number of  occurrences of a feature or the average distance between said occurrences. In particular, given the nature of the HIV CA models, three features in particular stand out: blobs, corners, and contours. The image features described below were identified at timesteps $t\in\{0,5,\ldots,195,200\}$. 

\textit{Blobs} can be described as large masses of similarly colored cells. figure \ref{fig: blobs} demonstrates what a blob looks like within one of the HIV CA models.
\begin{figure}[ht]
    \centering
    \includegraphics[width=\columnwidth]{images/blobimg.png}
    \caption{Blobs (highlighted by red borders) that can be detected by image processing libraries.}
    \label{fig: blobs}
\end{figure}
Once blobs have been located in the image using the various libraries, a few values were extracted. From the OpenCV library, the following features were extracted
\begin{itemize}[noitemsep,topsep=5pt]
\item the number of blobs with an area of at least $10$,
\item the average distances between any two blobs with an area of at least $10$,
\item the average size of blobs with an area of at least $10$.  
\end{itemize}
From the Sci-Kit Image library, blob features included \begin{itemize}[noitemsep,topsep=5pt]
    \item number of blobs located by \texttt{LogBlob},
    \item average size of blobs,
    \item variance in the $x$ coordinates of located blobs,
    \item variance in the $y$ coordinate of the located blobs.
\end{itemize}

\textit{Contours} can be described as the inner or outer edge of a shape formed by similarly colored cells. This shape, unlike a blob, is not filled in with uniformly colored cells, but contains completely different colors within its interior. An example demonstrating a contour can be found in figure \ref{fig: contours}. 
\begin{figure}[ht]
    \centering
    \includegraphics[width=\columnwidth]{images/contourimg.png}
    \caption{Contours (highlighted by red borders) that can be detected by image processing libraries.}
    \label{fig: contours}
\end{figure}
After locating the contours using the OpenCV library, the following values were obtained: 
\begin{itemize}[noitemsep,topsep=5pt]
    \item number of contours with an area of at least $10$,
    \item avg. area of contours with an area of at least $10$,
    \item number of contours with an area of at least $100$,
    \item avg. area of contours with an area of at least $100$,
    \item number of contours with perimeter of at least $10$,
    \item avg. area of contours with perimeter of at least $10$,
    \item number of contours with perimeter of at least $20$,
    \item avg. area of contours with perimeter of at least $20$,
    \item maximum area of all contours,
    \item maximum perimeter of all contours.
\end{itemize}

\textit{Corners} can be described as a sharp turn taken by a strip of similarly colored cells. figure \ref{fig: corner} shows the occurrence of several corners recognized by the Sci-Kit Learn library.
\begin{figure}[ht]
    \centering
    \includegraphics[width=\columnwidth]{images/cornersimg.png}
    \caption{Corners (highlighted by red borders) that can be detected by image processing libraries.}
    \label{fig: corner}
\end{figure}
Using this library, following values were obtained:
\begin{itemize}[noitemsep,topsep=0pt]
    \item number of corners found by the Harris function,
    \item distance between found corners.
\end{itemize}

\subsection{Regression}
With the image features collected, the data is nearly ready for regression. \textit{Regression} is the statistical practice of predicting some numerical measure of a new data point in a dataset, given the characteristics of said data point. The target measures, or \textit{labels}, for the HIV CA models are the percentages of each target class (healthy, dead, latently infected, and acute infected). From each of the simulations runs of all five HIV models, all 4 class percentages were calculated at the final timestep, $t = 600$, or 600 weeks after the onset of HIV in the human body. Once the composite data set of image features and target labels is created, regression can begin. The regressors choose a target label to predict, and are fed increasingly more information; at first, they have access to the data from $t=0$, then from both $t=0$ and $t=5$, and so on until the regressor has access to the data from all timesteps during which image information was collected. At the end of each regression task, an error rate is calculated using \textit{root mean squared error} (RMSE), which, for a sequence of data $\{y_i\}_{i=1}^{N}$, is given by \[
    \mathrm{RMSE}=\sqrt{\sum_{i=1}^{N}\frac{(y_i-\bar{y})}{N}},
\]
where $\bar y$ is the sample average. The RMSE is a positive value that dictates how far a regressors prediction is away from the target label. For instance, if the target label indicated that $7.3\%$ of the cells at the end of a simulation run were dead, and a regressor predicted that $6.1\%$ of the cells would be dead given information from timesteps $0$ through $100$, an RMSE value of $1.2$. In context, this would mean that this particular regressor will predict, on average, a percentage of dead cells that is off from the true percentage by $1.2\%$. 

The goal of regression is to lower the RMSE error without approaching an error of $0$. If this were to happen, then the model would not adapt well to new data points, and would be \textit{overfit} to the current dataset. Thus, a rigorous approach must be taken to lower error without overfitting. This process is a combination of \textit{nested cross-fold validation} (CFV) and \textit{hyperparameter} tuning (HT). When training a regression model on a dataset, there must be a subdivision within the dataset to \textit{test} the model on. It cannot be tested on data it has already seen; rather, the test set of data is not seen by the model until it is being tested for accuracy. This, to properly validate the accuracy of a model, CFV is utilized. This process breaks down the dataset into $k$ folds or ``chunks''. The model is first trained on the first $k-1$ folds, and tested on the $k$th fold. This process is repeated until all $k$ folds have been utilized as the test set of data. The \textit{nested} portion of this process comes from further subdividing the dataset one more time into a \textit{validation set}. This validation set is a subset of the training set already previously identified. Why validate the model before testing it? This comes back to HT. Hyperparameter tuning is a process by which the various parameters of a model are tuned to have the lowest error. In order to find which given set of parameters obtains the lowest error, each regressor must be validated. Listed below are the various models used and the hyperparameters that were tuned:
\begin{itemize}
    \item \textbf{Support Vector Machine}\quad This regressor used two different core functions, called kernels, to calculate its regressions. These kernels are the Radial Basis Function (RBF) and Polynomial kernels of degree $d$
    \begin{align*}
        &\text{RBF}&&K(\vb a,\vb b)=\exp(-\gamma\norm{\vb a-\vb b}^2), \\
        &\text{Polynomial}&&K(\vb a,\vb b)=(\gamma\vb a^T\vb b+r)^d.
    \end{align*}
    Sci-Kit Learn allows for many parameters in its SVR function, but our team found after testing that the \texttt{gamma}, \texttt{C}, \texttt{epsilon}, and \texttt{degree} values showed the most promise for getting low errors \cite{svm_pdf}.
    
    \item \textbf{Gaussian Process}\quad Much like the SVR, the GP regressor also has its own kernels. These are the RBF and Rational Quadratics (RQ) kernels. After preliminary testing, the \texttt{alpha} and \texttt{length\_scale} parameters also showed more promise than others.\footnote{The computational cost of the RQ kernel is significantly higher than the other regressors examined, and did not finish running in time. Only results from the RBF kernel is obtained and studied at the time of writing, but we plan on merging the two kernel's data as soon as they become available.}
    
    \item \textbf{Lasso}\quad Unlike previous regressors, this regressor does not have different kernels. Instead, the only viable hyperparameter is \texttt{alpha}.
    
    \item \textbf{Decision Tree}\quad Parameters \texttt{max\_depth} and \texttt{min\_samples\_split} were tested.
\end{itemize}
Once all parameter combinations have been validated within the \textit{inner fold} (the fold the CFV with the validation sets), then the regressor with the best parameter combination moves to the \textit{outer fold} to calculate an average error and standard deviation from this average error using the $k$ folds of test datasets. The results from this rigorous process are explored in Sections 3 and 4.