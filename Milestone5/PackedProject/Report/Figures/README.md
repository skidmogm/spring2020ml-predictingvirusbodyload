Included here are the images we used in the report. Note that all the figures will be contained in the source file directory of our report, so these are just a copy of those and put here for convenience. 

`CrossFoldValScores` contains all the Average RMSE versus week graph we plotted. The folder names from then on should be self explanatory enough (and in fact mirror the structure in the `Data` section of the project).
