import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Patch


classifiers=['tree','svr','gaussian_process', 'lasso'] #Can add any additional regressors here
names={'tree':'Tree', 'svr':'Support Vector Machine', 'gaussian_process':'Gaussian Processes', 'lasso':'Lasso'}
models=['dossantos','gonzalez','moonchai','precharattana','rana']

def plot(dir):
    fig = plt.figure(figsize=(11,7))
    fig.suptitle(names[dir], fontsize=16)
    for i in range(5):
        ax = fig.add_subplot(3,2,i+1)
        ax.set_title(str(models[i].capitalize()))
        ax.set_xlabel('T_bound')
        ax.set_ylabel('Avg RMSE')
        data_path= dir + '/' + models[i] + '.csv'
        data=pd.read_csv(data_path)
        health = data[0:41]
        dead = data[41:82]
        A1=data[82:123]
        A2=data[123:]
        ax.errorbar(health["T_bound"], health['score_avg'], health['score_stdev'], 
                 marker='.', markerfacecolor='green', markeredgecolor='green',
                 linestyle='None', ecolor='green', markersize=4, elinewidth=0.3,
                 capthick=0.35, capsize=1.25)
        ax.errorbar(dead["T_bound"], dead['score_avg'], dead['score_stdev'], 
                  marker='.', markerfacecolor='black', markeredgecolor='black',
                  linestyle='None', ecolor='black', markersize=4, elinewidth=0.3,
                  capthick=0.35, capsize=1.25)
        ax.errorbar(health["T_bound"], A1['score_avg'], A1['score_stdev'], 
                  marker='.', markerfacecolor='red', markeredgecolor='red',
                  linestyle='None', ecolor='red', markersize=4, elinewidth=0.3,
                  capthick=0.35, capsize=1.25)
        ax.errorbar(health["T_bound"], A2['score_avg'], A2['score_stdev'], 
                  marker='.', markerfacecolor='blue', markeredgecolor='blue',
                  linestyle='None', ecolor='blue', markersize=4, elinewidth=0.3,
                  capthick=0.35, capsize=1.25)
    legend_elements = [Patch(facecolor='green', edgecolor='green', label='Healthy'), 
				Patch(facecolor='black', edgecolor='black', label='Dead'),
				Patch(facecolor='red', edgecolor='red', label='Acute Infected (A1)'),
				Patch(facecolor='blue', edgecolor='blue', label='Latent Infected (A2)')]
    fig.legend(handles=legend_elements, loc='lower right')
    fig.tight_layout(pad=3.0)
    plt.savefig(str(dir) + '_plot.png')

if __name__== '__main__':
    for i in classifiers:
        plot(i)
    print('Plotting complete. ' + str(len(classifiers)) + ' output plots generated.')
