from sklearn.model_selection import ParameterGrid, KFold
from sklearn.linear_model import Lasso
import pandas as pd
import numpy as np

from IPython.display import clear_output
from itertools import product
import os

from concurrent.futures import ProcessPoolExecutor

def load_data(model):
    """ Helper function to load processed spreadsheet.

    This function drops useless columns and converts all cell counts
    to percentages.
    """

    data = pd.read_csv('clean_data/{}.csv'.format(model))

    # Convert cell counts to percentages
    data['_N_CELLS'] = data['L'] ** 2
    for col in ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']:
        data[col] = data[col] / data['_N_CELLS']

    # Drop useless columns. Incidentally, CELL_N is always zero
    # for all 900 runs for the moonchai model, so we drop that
    # column as well
    data = data.drop(['Model', 'Run', 'L', 'CELL_N', '_N_CELLS'], axis=1)

    return data

def nested_cv(X, y, inner_cv, outer_cv, Classifier, parameter_grid):
    outer_scores = []  # List of RMSE scores
    params_out = []  # List saving the best_params for each outer fold

    for training_samples, test_samples in outer_cv.split(X, y):
        best_params = {}
        best_score = -np.inf
        for parameters in parameter_grid:
            cv_scores = []
            for inner_train, inner_test in inner_cv.split(
                    X.loc[training_samples], y[training_samples]):
                clf = Classifier(**parameters)
                clf.fit(X.loc[inner_train], y[inner_train])
                score = clf.score(X.loc[inner_test], y[inner_test])
                cv_scores.append(score)
            mean_score = np.mean(cv_scores)
            if mean_score > best_score:
                best_score = mean_score
                best_params = parameters
        params_out.append(best_params)
        clf = Classifier(**best_params)
        clf.fit(X.loc[training_samples], y[training_samples])
        y_predict = clf.predict(X.loc[test_samples])
        outer_scores.append(
            np.sqrt(np.mean((y[test_samples] - y_predict) ** 2)))

    return outer_scores, params_out

def thread_main(model):
    data = load_data(model)
    y_cols = ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']
    X_cols = [col for col in data.columns if col not in y_cols]

    # Open file
    fout = open('lasso/{}.csv'.format(model), 'w')

    # Headers to write
    headers = ['Model', 'Class', 'T_bound']
    headers.extend(['score' + str(i) for i in range(1, 11)])
    headers.extend(['score_avg', 'score_stdev'])
    headers.extend([head + str(i) for i in range(1, 11) for head in ['alpha']])
    fout.write(','.join(headers) + '\n')
    fout.flush()

    parameters = ParameterGrid({'alpha': [1e-15,1e-7,1,55,100]})

    for y in y_cols:
        for t in range(1, 42):
            this_X_cols = X_cols[:20*t]

            scores, param = nested_cv(data[this_X_cols], data[y], KFold(n_splits=10), KFold(n_splits=10), Lasso, parameters)
            row = [model, y, 5 * (t - 1)]
            row.extend([scores[i] for i in range(10)])
            row.extend([np.average(scores), np.std(scores)])
            row.extend([param[i][k] for i in range(10)
                        for k in ['alpha']])
            row = list(map(lambda x: str(x), row))
            fout.write(','.join(row) + '\n')
            fout.flush()

if __name__ == "__main__":
    models = ['rana', 'dossantos', 'moonchai', 'precharattana', 'gonzalez']
    with ProcessPoolExecutor(max_workers=len(models)) as exe:
        for model in models:
            exe.submit(thread_main, model)