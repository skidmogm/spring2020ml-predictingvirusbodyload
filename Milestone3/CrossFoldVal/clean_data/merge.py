import pandas as pd

dfs = []
for model in ['rana', 'moonchai', 'precharattana', 'dossantos']:
    dfs.append(pd.read_csv(model + '.csv'))

pd.concat(dfs, ignore_index=True).to_csv('data.csv', index=False)

