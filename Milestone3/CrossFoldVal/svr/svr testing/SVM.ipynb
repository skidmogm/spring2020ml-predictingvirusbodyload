{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 style=\"text-align: center; color: blue;\">Milestone 3: SVR Testing</h1>\n",
    "<p style=\"text-align: center;\">It's time for us to really focus in on these regressors and get the hyperparameter tuning down so that we have overall better models. Here, in this Notebook, we will examine some promising ranges and parameters for the <b>Support Vector Regressor.</b></p>\n",
    "<br>\n",
    "<p style=\"text-align: center;\">First, we need to create our parameter grids...</p>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hide_input": true
   },
   "source": [
    "## Part 1: Parameter Grids\n",
    "\n",
    "The following parameter grids were taken from the following sources:<br>\n",
    "+ <a href=https://stats.stackexchange.com/questions/43943/which-search-range-for-determining-svm-optimal-c-and-gamma-parameters> Stack Exchange on Parameter Searching in SVM's</a> <i>(Which lead us to some of the later papers)</i>\n",
    "+ <a href=https://www.csie.ntu.edu.tw/~cjlin/papers/guide/guide.pdf>A Practical Guide to Support Vector Classification</a> <i>(Which described some good ranges for the <b>'C'</b> and </b>'gamma'</b> parameters)</i>\n",
    "+ <a href=http://kernelsvm.tripod.com/>Support Vector Machine Regression</a> <i>(Provided a basic overview of the RBF kernel)</i>\n",
    "+ <a href=https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVR.html>SVR Documentation</a>\n",
    "+ Various tests from Milestone 2 from the \"Regressor Testing (dos Santos)\" Notebook <i>(Which described many of the kernels and how we could use them)</i>\n",
    "\n",
    "\n",
    "After some reading, here were the original parameter ranges taken from the resources:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "param_grid = [{\"kernel\": [\"rbf\"], \"gamma\": [2e-15, 2e-13, 2e-11, 2e-9, 2e-7, 2e-5, 2e-3, 2e-1, 2, 2e3],\n",
    "               \"C\": [2e-5, 2e-3, 2e-1, 2, 2e3, 2e5, 2e7, 2e9, 2e11, 2e13, 2e15],\n",
    "               \"epsilon\": [2e-3, 2e-1, 2, 2e3]},\n",
    "              \n",
    "              {\"kernel\": [\"poly\"], \"degree\": [1,2,3,5,10], \"coef0\": [2e-5, 2e-3, 2e-1, 2, 2e3, 2e5, 2e7, 2e9, 2e11, 2e13, 2e15],\n",
    "               \"gamma\": [2e-15, 2e-13, 2e-11, 2e-9, 2e-7, 2e-5, 2e-3, 2e-1, 2, 2e3],\n",
    "                \"C\": [2e-5, 2e-3, 2e-1, 2, 2e3, 2e5, 2e7, 2e9, 2e11, 2e13, 2e15],\n",
    "                \"epsilon\": [2e-3, 2e-1, 2, 2e3]}]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"text-align:center;\">Why did we choose these values? Besides reading the paper itself, there were also some formulas we relied on in Chapter 5 of the textbook. Here, it listed out the individual formulae for the kernels themselves.</p>\n",
    "<br>\n",
    "<img src=\"Sv Kernels.png\">\n",
    "<br>\n",
    "<p style=\"text-align:center;\">We can see that the <b>Polynomial kernel</b> uses <i>'gamma', 'degree', </i>and<i> 'coef0'.</i> The <b>RBF kernel</b> only uses <i>'gamma'.</i></p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the purposes of testing, we don't actually need to test the entire range of all of these. This is for a few reasons:\n",
    "1. C and Gamma are both inherently necessary for the SVR to fit to the data, as they describe how <i>closely</i> a particular curve fits with all of the datapoints. Thus, we don't need to test them as vigorously as other ones. (Remember, each new parameter value we add exponentially increases the number of kfold validation we do.) Thus, we will only be testing the <i>extremes</i>.\n",
    "2. We don't need higher ranges of values for degree because of the <b>kernel trick</b>, which essentially means that lower order functions can replicate the viability of higher order functions. This same idea applies to the 'coef0' value, which means we can lower the ranges of those values drastically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# here is our new param_grid\n",
    "param_grid = [{\"kernel\": [\"rbf\"], \"gamma\": [2e-15, 2e-5, 2e3],\n",
    "               \"C\": [2e-5, 2, 2e15],\n",
    "               \"epsilon\": [2e-3, 2e-1, 2, 2e3]},\n",
    "              # the rbf kernel has 36 (3*3*4) parameter combinations\n",
    "              \n",
    "              {\"kernel\": [\"poly\"], \"degree\": [1,2,3], \"coef0\": [2e-3, 2, 2e3],\n",
    "               \"gamma\": [2e-15, 2e-5, 2e3],\n",
    "               \"C\": [2e-5, 2, 2e15],\n",
    "               \"epsilon\": [2e-3, 2, 2e3]}]\n",
    "              # the poly kernel has 162 (2*3*3*3) parameter combinations... this is still a lot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's cut it down one more time, because my poor poor computer cannot handle more... <br>\n",
    "Based on the kernel trick and the values listed in the paper, we can assume that the 'gamma', 'c', and 'epsilon' ranges listed above should be sufficient to tune up our models. Let's instead focus our energy on the 'degree' and 'coef0' values to determine if they truly deviate from the default values. This means we will no longer be looking at the <b>rbf</b> kernel, since its values have been proven in other papers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "param_grid = {\"kernel\": [\"poly\"], \"degree\": [1,2,3], \"coef0\": [2e-3, 2, 2e3], \"gamma\": ['auto']}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 2: Tuning\n",
    "\n",
    "The goal of this part is to tune each of the <i>many, many</i> parameter sets to get an idea of where to put the parameter ranges, or if we should even include that hyperparameter in our tuning for the larger Milestone 3 goal. <br>\n",
    "<b>Remember:</b> if we can eliminate any unnecessary or less useful hyperparameters, that would be ideal. We have several thousand nested cross-fold validations to run on <i>each</i> model, and every hyperparameter value we add is exponentially more computational time we need to use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# for nested_cv\n",
    "from sklearn.model_selection import ParameterGrid, KFold\n",
    "\n",
    "# support vector machine\n",
    "from sklearn.svm import SVR\n",
    "\n",
    "# important imports\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "\n",
    "# importing some of Junjiang's wonderful code\n",
    "from svr import load_data, nested_cv\n",
    "import json"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "def tune(model):\n",
    "    data = load_data(model)\n",
    "    y_cols = ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']\n",
    "    X_cols = [col for col in data.columns if col not in y_cols]\n",
    "\n",
    "    # Open file\n",
    "    fout = open('svr_tests.csv'.format(model), 'w')\n",
    "\n",
    "    # number of splits in our validation\n",
    "    # for the purposes of this testing, we will only be doing 3 splits for the KFold validation\n",
    "    folds = 5\n",
    "    \n",
    "    #turn our parameter grid into an actual Parameter Grid object\n",
    "    params = ParameterGrid(param_grid)\n",
    "    \n",
    "    # Headers to write\n",
    "    headers = ['Model', 'Class', 'T_bound']\n",
    "    headers.extend(['score' + str(i) for i in range(1, folds+1)])\n",
    "    headers.extend(['score_avg', 'score_stdev'])\n",
    "    headers.extend([f'params_{i}' for i in range(1, folds+1)])\n",
    "    fout.write(','.join(headers) + '\\n')\n",
    "    fout.flush()\n",
    "\n",
    "    for y in y_cols:\n",
    "        # we will only be doing a few time steps\n",
    "#         for t in range(1, 42):\n",
    "        for t in [25,50,75,100,125,150,175,200,225,250,275]:\n",
    "            this_X_cols = X_cols[:20*t]\n",
    "            scores, param = nested_cv(data[this_X_cols], data[y], KFold(n_splits=folds), KFold(n_splits=folds), SVR, params)\n",
    "            row = [model, y, 5 * (t - 1)]\n",
    "            row.extend([scores[i] for i in range(folds)])\n",
    "            row.extend([np.average(scores), np.std(scores)])\n",
    "            row.extend([json.dumps(par).replace(',', '!') for par in param])           \n",
    "            row = list(map(lambda x: str(x), row))\n",
    "            fout.write(','.join(row) + '\\n')\n",
    "            fout.flush()\n",
    "        \n",
    "    fout.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "tune('dossantos')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 3: Analysis\n",
    "Let's look at the data we just generated to determine a range for our 'degree' and whether or not we should include 'coef0'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>params_1</th>\n",
       "      <th>params_2</th>\n",
       "      <th>params_3</th>\n",
       "      <th>params_4</th>\n",
       "      <th>params_5</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <td>0</td>\n",
       "      <td>{\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...</td>\n",
       "      <td>{\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...</td>\n",
       "      <td>{\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...</td>\n",
       "      <td>{\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...</td>\n",
       "      <td>{\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>1</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>2</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>3</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>4</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>5</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>6</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>7</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>8</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <td>9</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "      <td>{\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                            params_1  \\\n",
       "0  {\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...   \n",
       "1  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "2  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "3  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "4  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "5  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "6  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "7  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "8  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "9  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "\n",
       "                                            params_2  \\\n",
       "0  {\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...   \n",
       "1  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "2  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "3  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "4  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "5  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "6  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "7  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "8  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "9  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "\n",
       "                                            params_3  \\\n",
       "0  {\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...   \n",
       "1  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "2  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "3  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "4  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "5  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "6  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "7  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "8  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "9  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "\n",
       "                                            params_4  \\\n",
       "0  {\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...   \n",
       "1  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "2  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "3  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "4  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "5  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "6  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "7  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "8  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "9  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...   \n",
       "\n",
       "                                            params_5  \n",
       "0  {\"coef0\": 2000.0! \"degree\": 1! \"gamma\": \"auto\"...  \n",
       "1  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "2  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "3  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "4  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "5  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "6  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "7  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "8  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  \n",
       "9  {\"coef0\": 0.002! \"degree\": 1! \"gamma\": \"auto\"!...  "
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tests = pd.read_csv('svr_tests.csv')\n",
    "\n",
    "# we're gonna focus on just the params that are output, since that is the only thing we are interested in\n",
    "parameters = tests[['params_1','params_2','params_3','params_4','params_5']]\n",
    "parameters.head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pay close attention to the values of 'coef0' and 'degree'. They largely (except for a weird cut of data in the first row) have the <b>same</b> value. If we refer to the <a href=\"https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVR.html\">SciKit Learn documentation for SVR's</a>, the default value for 'coef0' is 0. Notice that of the three values we tested for the 'coef0' hyper parameters (<b>[2e-3, 2, 2e3]</b>), that the SVR's <b><i>overwhelmingly</i></b> leaned towards the one closest to the default value of 0. It seems that this hyper parameter does __not__ have any meaningful impact on our model, and testing it would take additional unnecessary time.<br><br>\n",
    "\n",
    "Additionally, we notice that the 'degree' value tends toward a lower degree more often than not. We should therefore focus our energy on those low values, since they tell a truer story, on account of the <b>kernel trick</b>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## TLDR; We will evaluate the lower ranges of 'epsilon', 'gamma', 'C', and 'degree' as hyperparameters, but will _not_ look into 'coef0' or any others as viable hyperparameters for our SVRs on this project"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
