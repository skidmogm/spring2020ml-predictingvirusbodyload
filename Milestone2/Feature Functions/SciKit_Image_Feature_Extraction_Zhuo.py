# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 23:22:37 2020

@author: octlab
"""

import numpy as np
from skimage.feature import blob_log

def getFeatures(im):
    """
    input im is an 2D array with a size of L*L.
    the expected output is a dictionary. 
    Keys of the dictionary is names of features and values represents numerical values of those features
    """
    def getBlobs(data):
        img=data
        blobs_log = blob_log(img, min_sigma=10, max_sigma=30, num_sigma=10, threshold=0.01/255)
        return blobs_log

    def getAvgSize(blobs):
        return np.mean(blobs[:,2])
    
    def getVar_x(blobs):
        return np.var(blobs[:,0])/len(blobs)
    
    def getVar_y(blobs):
        return np.var(blobs[:,1])/len(blobs)
    #im=process_img(im)
    keypoints = getBlobs(im)
    return {'blob_count':len(keypoints),'blob_size':getAvgSize(keypoints),'blob_varx':getVar_x(keypoints),'blob_vary':getVar_y(keypoints)}
