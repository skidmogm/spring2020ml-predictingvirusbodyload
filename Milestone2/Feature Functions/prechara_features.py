def extract_features(numpy_array):

    import numpy as np
    import cv2

    # Helper function that converts a raw simulation result into an image operable
    # by the OpenCV library.
    def gridToOpenCVImage(grid):
        # Precharattana Model constants
        CELL_H   = 0
        CELL_A2  = 1
        CELL_A0  = 2
        CELL_A1  = 4
        CELL_D   = 8

        mycmap = {
            CELL_H: [255, 255, 255],
            CELL_A2: [0, 255, 0],
            CELL_A0: [128, 128, 0],
            CELL_A1: [0, 128, 0],
            CELL_D: [0, 0, 0], 
        }

        names = {
            CELL_H: 'Healthy',
            CELL_A2: 'Stage 1 Infected',
            CELL_A0: 'Latent Infected',
            CELL_A1: 'Stage 2 Infected', 
            CELL_D: 'Dead', 
        }
        # Create a L by L by 3 np array
        res = np.zeros([*grid.shape, 3], dtype=np.uint8)

        # trnasform grid into a format with an rgb component
        for row in range(len(grid)):
            for col in range(len(grid[0])):
                # Gets the color in mycmap and reverse it to BGR
                res[row][col] = mycmap[grid[row][col]][::-1]

        return res

    def getAvgDistance(keypoints):
        if keypoints == []:
            return 0

        N = len(keypoints)
        res = 0

        for kpi in keypoints:
            for kpj in keypoints:
                x1, y1 = kpi.pt
                x2, y2 = kpj.pt
                res += np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

        return res / (2 * N)

    def getAvgSize(keypoints):
        if keypoints == []:
            return 0

        return np.average([kp.size for kp in keypoints])

    # Grabs all contour edges from an image and calculates the perimeter of the contours
    def contour_perim(im, min_perim):
        imOrig = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY);
        ret,thresh = cv2.threshold(imOrig,127,255,0)
        contours,hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[1:]      # First contour is the whole image, so it is removed

        cnt_perim = np.array(list(map(lambda x : cv2.arcLength(x,True), contours)))
        cnt_perim_filtered = cnt_perim[cnt_perim > min_perim]
        return cnt_perim_filtered if cnt_perim_filtered.size > 0 else [0]  

    # Grabs all contour edges from an image and calculates the area of the contours
    def contour_area(im, min_area):
        imOrig = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY);
        ret,thresh = cv2.threshold(imOrig,127,255,0)
        contours,hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[1:]     # First contour is the whole image, so it is removed
        
        cnt_area = np.array(list(map(lambda x : cv2.contourArea(x), contours)))
        cnt_area_filtered = cnt_area[cnt_area > min_area]
        return cnt_area_filtered if cnt_area_filtered.size > 0 else [0]

    # Helper function to run the SimpleBlobDetector algorithm on a given image.
    # Return all found blobs with minimum area selected by input.
    def getKeypoints(im, area):
        params = cv2.SimpleBlobDetector_Params()

        params.minThreshold = 1
        params.maxThreshold = 200

        params.filterByArea = True
        params.minArea = area

        params.filterByCircularity = False
        params.filterByConvexity = False
        params.filterByInertia = False

        detector = cv2.SimpleBlobDetector_create(params)
        return detector.detect(im)

    # Returns all the 6 features from each image. The format of return is a 1x6
    # numpy array with fields [num, dist, avg size, num, dist, avg size] where
    # the first 3 correspond to keypoints obtained when minArea = 5
    def getFeatures(im):
        keypoints_5 = getKeypoints(im, 5)
        keypoints_10 = getKeypoints(im, 10)

        return {'5_num': len(keypoints_5), '5_avg_dist': getAvgDistance(keypoints_5),
                '5_avg_size': getAvgSize(keypoints_5), '10_num': len(keypoints_10), 
                '10_avg_dist': getAvgDistance(keypoints_10), '10_avg_dist': getAvgSize(keypoints_10),
                'cnt_area_10': len(contour_area(im, 10)), 'cnt_area_100': len(contour_area(im, 100)),
                'cnt_perim_10' : len(contour_perim(im, 10)), 'cnt_perim_20' : len(contour_perim(im, 20)),
                'avg_area_10': np.mean(contour_area(im, 10)), 'avg_area_100': np.mean(contour_area(im, 100)),
                'avg_perim_10' : np.mean(contour_perim(im, 10)), 'avg_perim_20' : np.mean(contour_perim(im, 20)),
                'max_cnt_area' : max(contour_area(im, 0)), 'max_cnt_perim' : max(contour_perim(im, 0))}

    im = gridToOpenCVImage(numpy_array)
    features_dict = getFeatures(im)
    
    return features_dict