# Script containing a single top-level function that accepts a simulation grid
# as input and returns a dictionary of feature names and values like so -
# {
#   "5_num_blobs": 150;
#   "5_avg_blob_dist": 1000;
#   ...
# }


def getFeatures(grid):
    # Top level import statements
    import numpy as np
    import cv2  # Imports openCV

    # Numerically encoded cell states, after being processed by
    # the master script.
    CELL_H = 1  # Healthy
    CELL_A1 = 2  # Acute Infected
    CELL_A2 = 3  # Latent Infected
    CELL_D = 4  # Dead

    # Self-defined color map.
    mycmap = {
        CELL_H: [0, 255, 0],
        CELL_A1: [255, 0, 0],
        CELL_A2: [0, 0, 255],
        CELL_D: [0, 0, 0],
    }

    # Helper function that converts a raw simulation result into an image
    # operable by the OpenCV library.
    def gridToOpenCVImage(grid):
        # Create a L by L by 3 np array
        shape = grid.shape
        res = np.zeros((shape[0], shape[1], 3), dtype=np.uint8)

        # Note that openCV stores image in BGR format. The colors
        # in mycmap are given in RGB.
        for row in range(len(grid)):
            for col in range(len(grid[0])):
                # Gets the color in mycmap and reverse it to BGR
                res[row][col] = mycmap[grid[row][col]][::-1]

        return res

    # Helper function to compute the average distance among blobs
    def getAvgDistance(keypoints):
        if keypoints == []:
            return 0

        N = len(keypoints)
        res = 0

        for kpi in keypoints:
            for kpj in keypoints:
                x1, y1 = kpi.pt
                x2, y2 = kpj.pt
                res += np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

        return res / (2 * N)

    # Helper function to get the average size of the blobs
    def getAvgSize(keypoints):
        if keypoints == []:
            return 0

        return np.average([kp.size for kp in keypoints])

    # Helper function to run the SimpleBlobDetector algorithm on a given image.
    # Return all found blobs with minimum area selected by input.
    def getKeypoints(im, area):
        params = cv2.SimpleBlobDetector_Params()

        params.minThreshold = 1
        params.maxThreshold = 200

        params.filterByArea = True
        params.minArea = area

        params.filterByCircularity = False
        params.filterByConvexity = False
        params.filterByInertia = False

        detector = cv2.SimpleBlobDetector_create(params)
        return detector.detect(im)

    # Extract image features and return them
    keypoints_5 = getKeypoints(gridToOpenCVImage(grid), 5)
    keypoints_10 = getKeypoints(gridToOpenCVImage(grid), 10)

    return {
        "5_num_blobs": len(keypoints_5),
        "5_avg_blob_dist": getAvgDistance(keypoints_5),
        "5_avg_blob_size": getAvgSize(keypoints_5),
        "10_num_blobs": len(keypoints_10),
        "10_avg_blob_dist": getAvgDistance(keypoints_10),
        "10_avg_blob_size": getAvgSize(keypoints_10)
    }
