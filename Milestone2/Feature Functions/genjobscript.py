for model in ['dossantos', 'rana', 'precharattana', 'moonchai', 'gonzalez']:
    for L in ['800', '1000', '1200']:
        with open(model + L + '.job', 'w') as f:
            f.writelines([
                '#!/bin/bash -l\n',
                '#PBS -N ' + model + L + '\n',
                '#PBS -l nodes=1:ppn=4\n',
                '#PBS -l walltime=48:00:00\n',
                '#PBS -q batch\n',
                '#PBS -m abe\n',
                '#PBS -M lij111@miamioh.edu\n',
                '#PBS -j oe\n',
                '\n',
                'cd ~/spring2020ml-predictingvirusbodyload/Milestone2/data_generation\n',
                'source /software/python/anaconda3/etc/profile.d/conda.sh\n',
                '$CONDA_PYTHON_EXE ../features/' + model + L + '.py'
            ])
