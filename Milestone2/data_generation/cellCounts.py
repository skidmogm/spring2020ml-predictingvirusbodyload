import numpy as np
from numba import jit, prange
import os

# Function that converts grids with raw values into a uniform set of values
# for all models.
@jit(nopython=True)
def count(im, model):
    healthy, dead, A1, A2, cell_n = 0, 0, 0, 0, 0
    if model == 'dossantos':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    healthy += 1
                elif value == 4:
                    A1 += 1
                elif value == 1:
                    A2 += 1
                elif value == 8:
                    dead += 1
    elif model == 'moonchai':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    cell_n += 1
                elif value == 4:
                    healthy += 1
                elif value == 2:
                    A1 += 1
                elif value == 1:
                    A2 += 1
                elif value == 8:
                    dead += 1
    elif model == 'precharattana':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    healthy += 1
                elif value == 4:
                    A1 += 1
                elif value == 1:
                    A1 += 1
                elif value == 2:
                    A2 += 1
                elif value == 8:
                    dead += 1
    elif model == 'rana':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    healthy += 1
                elif value == 2:
                    healthy += 1
                elif value == 8:
                    healthy += 1
                elif value == 10:
                    healthy += 1
                elif value == 4:
                    A1 += 1
                elif value == 1:
                    A2 += 1
                elif value == 16:
                    dead += 1
    elif model == 'gonzalez':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0 or value == 1 or value == 2 or value == 3:
                    healthy += 1
                elif value == 84:
                    A1 += 1
                elif value == 21:
                    A2 += 1
                elif value == 4:
                    dead += 1
    return healthy, dead, A1, A2, cell_n


# Crawl through the directories
# models = ['rana', 'gonzalez', 'moonchai', 'dossantos', 'precharattana']
models = ['gonzalez', 'moonchai']
BASE_DIR = os.getcwd()

for model in models:
    os.chdir(BASE_DIR)
    # If the model is not here (as in the case for starting treatment
    # early, we gracefully continue)
    if not os.path.isdir(model):
        continue

    fout = open(f'{model}_cell_counts.csv', 'w')
    fout.write(','.join(['Model', 'L', 'Run', 'T', 'CELL_H',
                         'CELL_D', 'CELL_A1', 'CELL_A2', 'CELL_N']) + '\n')

    for L in ['800', '1000', '1200']:
        for run in range(1, 301):
            for t in range(0, 201, 5):
                os.chdir(os.path.join(BASE_DIR, model, L, str(run)))
                healthy, dead, A1, A2, cell_n = count(
                    np.load(f'{t}.npz')['arr_0'], model)
                row = [model, L, run, t, healthy, dead, A1, A2, cell_n]
                row = list(map(lambda x: str(x), row))
                fout.write(','.join(row) + '\n')
                fout.flush()
    fout.close()
