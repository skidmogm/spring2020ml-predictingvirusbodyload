import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.gaussian_process import GaussianProcessRegressor as gpr
import sklearn.gaussian_process.kernels as k
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import cross_val_score, train_test_split

from itertools import product
from concurrent.futures import ProcessPoolExecutor
import json


def load_data(model):
    """ Helper function to load processed spreadsheet.

    This function drops useless columns and converts all cell counts
    to percentages.
    """

    data = pd.read_csv('clean_data/{}.csv'.format(model))

    # Convert cell counts to percentages
    data['_N_CELLS'] = data['L'] ** 2
    for col in ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']:
        data[col] = data[col] / data['_N_CELLS']

    # Drop useless columns. Incidentally, CELL_N is always zero
    # for all 900 runs for the moonchai model, so we drop that
    # column as well
    data = data.drop(['Model', 'Run', 'L', 'CELL_N', '_N_CELLS'], axis=1)

    return data


def main(model):
    """ Main function for each subprocess.
    This function is a parallel wrapper for all code written in the 
    jupyter notebook. In particular, this function only evaluates 
    one model at a time.
    """
    # List of regressor objects the loop will use. This is the only place
    # that needs to be changed if a new regressor needs to be used.
    regressors = {
        'tree': DecisionTreeRegressor(min_samples_leaf=2),

        'svr': Pipeline([
            ('scale', StandardScaler()),

            # LinearSVR seems to have worse convergence properties than SVR.
            # Since our dataset is small, we can afford to use SVR.
            # Epsilon is set to low since the targets \in [0, 1].
            ('svr', SVR(kernel='poly', degree=1, epsilon=1e-3))
        ]),

        'gpr': gpr(kernel=k.RationalQuadratic(alpha=.1, length_scale=1))
    }

    # Sets up an empty dictionary for storage.
    err_dict = {
        model: {}
    }

    # Generates pairs of configurations (model name, name of regressor to run)
    # Using this instead of nested for loops because double indentations just
    # to get the loop running is pretty nasty looking.
    configs = list(product(
        [model],
        list(regressors.keys())
    ))

    # Main loop that loops through all models and regressors, and performs
    # all necessary computations.

    for config in configs:
        # Expands the ordered pair and extract model and regressor_name
        # and also grabs the regressor object
        model, regressor_name = config
        regressor = regressors[regressor_name]

        # Load data and create a new entry for the model in err_dict
        data = load_data(model)
        err_dict[model][regressor_name] = {}

        # Class columns and feature columns
        y_cols = ['CELL_H', 'CELL_D', 'CELL_A1', 'CELL_A2']
        X_cols = [col for col in data.columns if col not in y_cols]

        for col in y_cols:
            # Printing progress so we have an idea where we are
            print('Now calculating', model, 'model with regressor',
                  regressor_name, 'on', col)

            # Stores the average of the cv scores, will be saved
            # to err_dict
            errors = []

            y = data[col]

            for t in range(1, 42):
                # Grabbing features up to t = some multiple of five
                # This 20 here is because we have 20 features for each
                # time step.
                this_X_cols = X_cols[:20*t]
                X = data[this_X_cols]

                scores = cross_val_score(
                    regressor, X, y, scoring='neg_mean_squared_error', cv=10)
                errors.append(np.average(np.sqrt(-scores)))

            # Saves error to the appropriate place
            err_dict[model][regressor_name][col] = errors

    print(model, 'done!')
    return err_dict


if __name__ == '__main__':
    err_dict = {}
    futures = []

    # Spawn children to work for us.
    with ProcessPoolExecutor(max_workers=4) as exe:
        for model in ['rana', 'dossantos', 'moonchai', 'precharattana']:
            futures.append(exe.submit(main, model))

    for ftr in futures:
        err_dict.update(ftr.result())

    # Dump to json so we can read it in later.
    with open('res.json', 'w') as fout:
        json.dump(err_dict, fout)
