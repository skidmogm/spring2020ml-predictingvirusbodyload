import logging
import datetime
import os

import numpy as np
from numba import jit, prange

import time
import sys
from IPython.display import clear_output

from subprocess import Popen

# Set up things for logging
ROOT_DIR = os.getcwd()
LOG_FILE = os.path.join(ROOT_DIR, 'records.log')
logging.basicConfig(filename=LOG_FILE, level=logging.INFO)

# These lines of code are not necessary when there is only one
# simulation file, but it doesn't hurt.
fileh = logging.FileHandler(LOG_FILE, 'a')
formatter = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
fileh.setFormatter(formatter)

log = logging.getLogger()  # root logger
for hdlr in log.handlers[:]:  # remove all old handlers
    log.removeHandler(hdlr)
log.addHandler(fileh)      # set the new handler


# Helper function to print a progress bar to jupyter notebook.
def update_progress(progress, name='Progress'):
    # Progress bar from https://www.mikulskibartosz.name/how-to-display-a-progress-bar-in-jupyter-notebook/
    bar_length = 20
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
    if progress < 0:
        progress = 0
    if progress >= 1:
        progress = 1

    block = int(round(bar_length * progress))

    clear_output(wait=True)
    text = "{2}: [{0}] {1:.1f}%".format(
        "#" * block + "-" * (bar_length - block), progress * 100, name)
    print(text)


# Constants for the model
# (Cell states picked for their binary values)
CELL_H = 0
CELL_Ht1 = 2  # therapy 1
CELL_Ht2 = 8  # therapy 2
CELL_HtB = 10  # dual therapy
CELL_D = 16
CELL_A1 = 4
CELL_A2 = 1

# Step functions for the model
# 1) non-therapy step
# 2) therapy step


# @jit(nopython=True, parallel=True)
@jit(nopython=True, parallel=True)
def m3Adh_step(grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau1):

    # variables to count the number of cells in different states
    inf_A1 = 0
    inf_A2 = 0
    dead = 0

    for i in prange(1, grid.shape[0]-1):
        for j in range(1, grid.shape[1]-1):
            cell = grid[i, j]

            if cell == CELL_H:

                # healthy cell can be infected by 1 A1 neighbor or 4 A2 neighbors (with a chance of resisting infection)
                if ((grid[i-1, j-1] & 5) + (grid[i-1, j] & 5) + (grid[i-1, j+1] & 5) +
                    (grid[i,  j-1] & 5) + (grid[i,  j+1] & 5) +
                        (grid[i+1, j-1] & 5) + (grid[i+1, j] & 5) + (grid[i+1, j+1] & 5)) >= 4:

                    if np.random.random() <= P_i:
                        buffer[i, j] = CELL_A1

                # or else the cell can be infected by a free-virion
                if np.random.random() <= P_v:
                    buffer[i, j] = CELL_A1

            elif cell == CELL_A1:
                inf_A1 += 1
                taugrid[i, j] += 1
                if taugrid[i, j] == tau1:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif cell == CELL_A2:
                inf_A2 += 1
                buffer[i, j] = CELL_D

            else:  # elif value == CELL_D:
                dead += 1
                randNum = np.random.random()

                if randNum < P_repH:
                    if randNum <= (P_repH * P_repI):
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_H

    return (grid.shape[0]-2)*(grid.shape[1]-2)-inf_A1-inf_A2-dead, 0, 0, 0, inf_A1, inf_A2, dead

# @jit(nopython=True, parallel=True)
@jit(nopython=True, parallel=True)
def m3Adh_tStep(grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau1, P_T1, P_infT1, P_T2, P_infT2, P_adh):

    # variables to count the number of cells in different states
    healthyT1 = 0
    healthyT2 = 0
    healthyTB = 0
    inf_A1 = 0
    inf_A2 = 0
    dead = 0

    for i in prange(1, grid.shape[0]-1):
        for j in range(1, grid.shape[1]-1):
            cell = grid[i, j]

            if cell == CELL_H:

                randNum1 = np.random.random()
                randNum2 = np.random.random()
                randNum3 = np.random.random()

                # Cell can receive therapy 1
                if (randNum1 <= P_T1) and (randNum2 > P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht1
                    continue

                # Cell can receive only therapy 2
                elif (randNum1 > P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht2
                    continue

                # Cell can receive both therapies
                elif (randNum1 <= P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_HtB
                    continue

                # healthy cell can be infected by 1 A1 neighbor or 4 A2 neighbors (with a chance of resisting infection)
                elif (((randNum1 <= P_i) and
                        (((grid[i-1, j-1] & 5) + (grid[i-1, j] & 5) + (grid[i-1, j+1] & 5) +
                          (grid[i,  j-1] & 5) + (grid[i,  j+1] & 5) +
                          (grid[i+1, j-1] & 5) + (grid[i+1, j] & 5) + (grid[i+1, j+1] & 5)) >= 4)) or
                        (randNum2 <= P_v)):

                    buffer[i, j] = CELL_A1
                    continue

            elif cell == CELL_Ht1:
                healthyT1 += 1

                randNum1 = np.random.random()
                randNum2 = np.random.random()
                randNum3 = np.random.random()

                # Cell can receive both therapies
                if (randNum1 <= P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_HtB
                    continue

                elif ((randNum1 <= P_infT1) and (((randNum1 <= P_i) and
                                                  (((grid[i-1, j-1] & 5) + (grid[i-1, j] & 5) + (grid[i-1, j+1] & 5) +
                                                    (grid[i,  j-1] & 5) + (grid[i,  j+1] & 5) +
                                                    (grid[i+1, j-1] & 5) + (grid[i+1, j] & 5) + (grid[i+1, j+1] & 5)) >= 4)) or
                                                 (randNum2 <= P_v))):

                    buffer[i, j] = CELL_A1
                    continue

                # Cell can receive only therapy 2
                elif (randNum1 > P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht2
                    continue

                # or else cell become normal healthy
                elif (randNum1 > P_T1 and randNum2 > P_T2):
                    buffer[i, j] = CELL_H

            elif cell == CELL_Ht2:
                healthyT2 += 1

                randNum1 = np.random.random()
                randNum2 = np.random.random()
                randNum3 = np.random.random()

                # Cell can receive both therapies
                if (randNum1 <= P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_HtB
                    continue

                elif ((randNum1 <= P_infT2) and (((randNum1 <= P_i) and
                                                  (((grid[i-1, j-1] & 5) + (grid[i-1, j] & 5) + (grid[i-1, j+1] & 5) +
                                                    (grid[i,  j-1] & 5) + (grid[i,  j+1] & 5) +
                                                    (grid[i+1, j-1] & 5) + (grid[i+1, j] & 5) + (grid[i+1, j+1] & 5)) >= 4)) or
                                                 (randNum2 <= P_v))):

                    buffer[i, j] = CELL_A1
                    continue

                # Cell can receive therapy 1
                elif (randNum1 <= P_T1) and (randNum2 > P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht1
                    continue

                # or else cell becomes normal healthy
                elif (randNum1 > P_T1 and randNum2 > P_T2):
                    buffer[i, j] = CELL_H

            elif cell == CELL_HtB:
                healthyTB += 1
                buffer[i, j] = CELL_H

            elif cell == CELL_A1:
                inf_A1 += 1
                taugrid[i, j] += 1
                if taugrid[i, j] == tau1:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif cell == CELL_A2:
                inf_A2 += 1
                buffer[i, j] = CELL_D

            else:  # elif value == CELL_D:
                dead += 1
                randNum = np.random.random()

                if randNum < P_repH:
                    if randNum <= (P_repH * P_repI):
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_H

    return (grid.shape[0]-2)*(grid.shape[1]-2)-healthyT1-healthyT2-healthyTB-inf_A1-inf_A2-dead, healthyT1, healthyT2, healthyTB, inf_A1, inf_A2, dead


def run(timesteps=600, beginTherapy=20, P_adh=1.0, replications=1000, L=100, P_HIV=0.05, P_i=0.997, P_v=.00001,
        P_repH=.99, P_repI=.00001, tau=4, P_T1=0.7, P_infT1=0.07, P_T2=0.5, P_infT2=0.05):
    """
    Runs the model.
    """
    
    # Saves the current date & time (and replace all : by .)
    # This datetime object will be used as the name the directory that
    # contains all the simulation data, as well as in the records log.
    # The directory structure of the simulation is as follows:
    #
    # d_str (folder, created on calling the run function)
    #   - 1 (folder, corresponding to replication 1)
    #     - 0.npy
    #     - 1.npy
    #     ...
    #   - 2 (folder, corresponding to replication 2)
    #     - 0.npy
    #     - 1.npy
    #     ...
    #   ...
    d = datetime.datetime.today()
    d_str = d.strftime('%m-%d-%Y %H:%M:%S').replace(':', '.')
    
    # Logs information about this run, for look ups later.
    logging.info('{0:s},T={1:d},beginTherapy={10:d},Replications={2:d},L={3:d},P_HIV={4:s},P_i={5:s},P_v={6:s},'
                 'P_repH={7:s},P_repI={8:s},tau={9:s},P_T1={11:s},P_infT1={12:s},P_T2={13:s},P_infT2={14:s},P_adh={15:s}'.format(
                     d_str, timesteps, replications, L,
                     '{:f}'.format(P_HIV).rstrip('0'),
                     '{:f}'.format(P_i).rstrip('0'),
                     '{:f}'.format(P_v).rstrip('0'),
                     '{:f}'.format(P_repH).rstrip('0'),
                     '{:f}'.format(P_repI).rstrip('0'),
                     str(tau),
                     beginTherapy,
                     '{:f}'.format(P_T1).rstrip('0'),
                     '{:f}'.format(P_infT1).rstrip('0'),
                     '{:f}'.format(P_T2).rstrip('0'),
                     '{:f}'.format(P_infT2).rstrip('0'),
                     '{:f}'.format(P_adh).rstrip('0')))

    # Create new directory to store simulation results.
    # Name the new directory based on the date's string representation
    RUN_DIR = os.path.join(ROOT_DIR, d_str)
    os.mkdir(RUN_DIR)
    os.chdir(RUN_DIR)

    # Dump a txt file in this directory also for further information
    # about the parameters of this run
    with open('params.txt', 'w') as f:
        f.write('Simulation started at ' + d_str + '.\n\n')
        f.write('Parameters:\n')
        f.write('\tTimesteps:    {:d}\n'.format(timesteps))
        f.write('\tBeginTherapy: {:d}\n'.format(beginTherapy))
        f.write('\tReplications: {:d}\n'.format(replications))
        f.write('\tGrid size:    {:d}\n'.format(L))
        f.write('\tP_HIV:        {:s}\n'.format(
            '{:f}'.format(P_HIV).rstrip('0')))
        f.write('\tP_i:          {:s}\n'.format(
            '{:f}'.format(P_i).rstrip('0')))
        f.write('\tP_v:          {:s}\n'.format(
            '{:f}'.format(P_v).rstrip('0')))
        f.write('\tP_repH:       {:s}\n'.format(
            '{:f}'.format(P_repH).rstrip('0')))
        f.write('\tP_repI:       {:s}\n'.format(
            '{:f}'.format(P_repI).rstrip('0')))
        f.write('\ttau:          {:s}\n'.format(
            str(tau)))
        f.write('\tP_T1:         {:s}\n'.format(
            '{:f}'.format(P_T1).rstrip('0')))
        f.write('\tP_infT1:      {:s}\n'.format(
            '{:f}'.format(P_infT1).rstrip('0')))
        f.write('\tP_T2:         {:s}\n'.format(
            '{:f}'.format(P_T2).rstrip('0')))
        f.write('\tP_infT2:      {:s}\n'.format(
            '{:f}'.format(P_infT2).rstrip('0')))
        f.write('\tP_adh:        {:s}\n'.format(
            '{:f}'.format(P_adh).rstrip('0')))

    update_progress(0)

    @jit
    def init_grid():
        grid = np.zeros((L+2, L+2), dtype=np.uint8)

        for i in range(1, grid.shape[0]-1):
            for j in range(1, grid.shape[1]-1):
                if np.random.random() <= P_HIV:
                    grid[i, j] = CELL_A1

        return grid, np.zeros_like(grid)
    
    # Prepare memory to store the prevelances of cell types (healthy
    # or dead, etc) at the t=600 stage for every replication of the
    # simulation.
    prevelance = np.zeros([replications, 7])

    for run in range(replications):
        # Allocate memory for all frames in the simulation
        # Will be saved to disk later.
        frames = np.zeros([timesteps + 1, L + 2, L + 2])

        grid, taugrid = init_grid()
        buffer = grid.copy()

        # Saves the state at t=0
        frames[0, :, :] = grid[:, :]

        for t in range(timesteps):
            # if random value is greater than P_adh, no therapy is taken
            if t < beginTherapy:
                healthy, healthy_t1, healthy_t2, healthy_tb, inf_A1, inf_A2, dead = m3Adh_step(
                    grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau)

            # else the therapy is taken
            else:
                healthy, healthy_t1, healthy_t2, healthy_tb, inf_A1, inf_A2, dead = m3Adh_tStep(
                    grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau, P_T1, P_infT1, P_T2, P_infT2, P_adh)

            grid[:, :] = buffer[:, :]
            
            # Saves the current grid to memory
            frames[t+1, :, :] = grid[:, :]

        # Dump all the 601 grids.
        DUMP_DIR = os.path.join(RUN_DIR, str(run + 1))
        
        # This function dumps raw numpy array (without compression) and
        # then calls gnu-tar and lbzip2 to compress the raw arrays after
        # every 20 writes. This can work on linux (but you have to change
        # the gtar to tar in the function definition) and also macOS
        # (where you need to install homebrew first, and then install
        # gnu-tar and lbzip2 from homebrew. No further changes requried.)
        # I'm using lbzip2 since it's a multithreaded compressor, which is
        # significantly faster than np.savez_compressed.
        saveFrames(frames, timesteps, DUMP_DIR)
        
        # # Uncomment the lines below to use regular old np.savez_compressed
        # # to save arrays to file.
        # os.mkdir(DUMP_DIR)
        # os.chdir(DUMP_DIR)
        # for frame, num in zip(frames, range(timesteps + 1)):
        #     np.savez_compressed(str(num), frame)
        #     update_progress((num + 1) / (timesteps + 1),
        #                     name='IO for run ' + str(run + 1))
        # clear_output(wait=True)
        # os.chdir(RUN_DIR)  # Exit
        
        os.chdir(RUN_DIR)
        
        # Saves prevelance of states to memory
        prevelance[run, :] = np.array(
            [healthy, healthy_t1, healthy_t2, healthy_tb, inf_A1, inf_A2, dead])

        update_progress((run + 1) / replications)
    
    # Write the prevelances to disk as a CSV
    with open('prevelance.csv', 'w') as f:
        f.write('H,H_T1,H_T2,H_Tb,A1,A2,D\n')
        for row in prevelance:
            f.write(str(row[0]) + ',' + str(row[1]) + ',' +
                    str(row[2]) + ',' + str(row[3]) + ',' +
                    str(row[4]) + ',' + str(row[5]) + ',' +
                    str(row[6]) + '\n')

    # return results_H, results_HT1, results_HT2, results_HTB, results_A1, results_A2, results_D

    os.chdir(ROOT_DIR)
    print('Simulation results saved under the directory "' + d_str + '".')


def saveFrames(frames, timesteps, save_dir, block_size=20):
    os.mkdir(save_dir)
    os.chdir(save_dir)

    children = []
    
    # Do not compress the t=0 frame.
    np.save('0', frames[0])
    
    # For every frame in t=1 to t=600, write them using np.save().
    # Every 20 runs (or some other number specified by block_size),
    # the script askes the OS to execute the gtar ... command to
    # compress all the written files. Once compressed, the original
    # files will be deleted.
    for frame, num in zip(frames[1:], range(1, timesteps + 1)):
        if ((num - 1) % block_size == 0) and (not num == 1):
            # num == 11, 21, 31, etc. Compress files 1-10, 11-20, etc.
            zip_name = str(num - block_size) + '-' + str(num - 1) + '.tar.bz2'
            files = [str(i) + '.npy' for i in range(num - block_size, num)]
            children.append(Popen(
                ['gtar', 'cf', zip_name, *files,
                 '--use-compress-program=lbzip2', '--remove-files']
            ))

        np.save(str(num), frame)

    # Compress the last few files
    zip_name = str(timesteps - block_size + 1) + \
        '-' + str(timesteps) + '.tar.bz2'
    files = [str(i) + '.npy' for i in
             range(timesteps - block_size + 1, timesteps)]
    children.append(Popen(
        ['gtar', 'cf', zip_name, *files,
         '--use-compress-program=lbzip2', '--remove-files']
    ))
    
    # Wait for all the child processes spawned to finish.
    for child in children:
        child.wait()

    # Keep the end for easier inspection
    np.save('600', frames[-1])
    
    # Exit the current directory
    os.chdir(os.pardir)

# Will not be triggered when imported from the notebook.
if __name__ == '__main__':
    run(L=1000, replications=1)
