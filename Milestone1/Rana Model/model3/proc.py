from IPython.display import clear_output
from IPython import display
from concurrent.futures import ProcessPoolExecutor
from concurrent.futures import ThreadPoolExecutor
import pandas as pd
import cv2
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.colors
import scipy.misc as smp
import numpy as np
import os
PROJECT_ROOT_DIR = os.getcwd()


# Numerically encoded cell states
CELL_H = 0  # Healthy
CELL_Ht1 = 2  # Therapy 1
CELL_Ht2 = 8  # Therapy 2
CELL_HtB = 10  # Dual Therapy
CELL_D = 16  # Dead
CELL_A1 = 4  # Acute Infected
CELL_A2 = 1  # Latent Infected

# Self-defined color map for consistent coloring.
mycmap = {
    CELL_H: [0, 255, 0],
    CELL_D: [0, 0, 0],
    CELL_A1: [255, 0, 0],
    CELL_A2: [255, 255, 0],
    CELL_Ht1: [148, 0, 211],
    CELL_Ht2: [75, 0, 130],
    CELL_HtB: [0, 0, 255]
}

# Used for legends and stuff.
names = {
    CELL_H: 'Healthy',
    CELL_Ht1: 'Healthy w/ Treatment 1',
    CELL_Ht2: 'Healthy w/ Treatment 2',
    CELL_HtB: 'Healthy w/ Both Treatments',
    CELL_D: 'Dead',
    CELL_A1: 'Acute Infected',
    CELL_A2: 'Latent Infected'
}

# Helper function that converts a raw simulation result into an image operable
# by the OpenCV library.


def gridToOpenCVImage(grid):
    # Create a L by L by 3 np array
    res = np.zeros([*grid.shape, 3], dtype=np.uint8)

    # Note that openCV stores image in BGR format. The colors
    # in mycmap are given in RGB.
    for row in range(len(grid)):
        for col in range(len(grid[0])):
            # Gets the color in mycmap and reverse it to BGR
            res[row][col] = mycmap[grid[row][col]][::-1]

    return res


def getAvgDistance(keypoints):
    if keypoints == []:
        return 0

    N = len(keypoints)
    res = 0

    for kpi in keypoints:
        for kpj in keypoints:
            x1, y1 = kpi.pt
            x2, y2 = kpj.pt
            res += np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

    return res / (2 * N)


def getAvgSize(keypoints):
    if keypoints == []:
        return 0

    return np.average([kp.size for kp in keypoints])


# Helper function to run the SimpleBlobDetector algorithm on a given image.
# Return all found blobs with minimum area selected by input.
def getKeypoints(im, area):
    params = cv2.SimpleBlobDetector_Params()

    params.minThreshold = 1
    params.maxThreshold = 200

    params.filterByArea = True
    params.minArea = area

    params.filterByCircularity = False
    params.filterByConvexity = False
    params.filterByInertia = False

    detector = cv2.SimpleBlobDetector_create(params)
    return detector.detect(im)


# Returns all the 6 features from each image. The format of return is a 1x6
# numpy array with fields [num, dist, avg size, num, dist, avg size] where
# the first 3 correspond to keypoints obtained when minArea = 5
def getFeatures(im):
    keypoints_5 = getKeypoints(im, 5)
    keypoints_10 = getKeypoints(im, 10)

    return np.array([
        len(keypoints_5), getAvgDistance(keypoints_5), getAvgSize(keypoints_5),
        len(keypoints_10), getAvgDistance(
            keypoints_10), getAvgSize(keypoints_10),
    ])


def getSubDirs():
    return [os.path.join(os.getcwd(), d) for d in os.listdir()
            if os.path.isdir(os.path.join(os.getcwd(), d))
            and not d == '__pycache__']


def getFeaturesParallelWrapper(run_dir, run):
    os.chdir(run_dir)

    im = gridToOpenCVImage(np.load('0.npy'))
    return {run: getFeatures(im)}


# Helper method to run get the spread sheet for each simulation. This function is
# separately extracted for parallel processing.
def getSimDataFrameParallel(sim_dir):
    # Enter the directory containing all the replications of one simulation
    # All runs under this directory have the same parameters, but are repeated
    # for the sake of reducing standard error.
    os.chdir(sim_dir)

    # Read in the prevelance of states csv
    prevelance_df = pd.read_csv('prevelance.csv')
    # Create a blank dataframe to fill in values later
    feature_df = pd.DataFrame(np.zeros([len(getSubDirs()), 6]),
                              columns=['5_num', '5_avg_dist', '5_avg_size',
                                       '10_num', '10_avg_dist', '10_avg_size'])

    # For each run, load the t=0 np array and extract all features
    futures = []
    with ProcessPoolExecutor(max_workers=6) as executor:
        for run_dir, run in zip(getSubDirs(), range(len(getSubDirs()))):
            ftr = executor.submit(getFeaturesParallelWrapper, run_dir, run)
            futures.append(ftr)

    # Merge all dictionaries to form a big dictionary
    dic = futures[0].result()
    for ftr in futures[1:]:
        dic.update(ftr.result())

    # Update feature_df
    for row in range(len(dic)):
        feature_df.iloc[row] = dic[row]

    # Join the prevelance_df and feature_df on index, and append the result
    # to the master dataframe df
    return pd.merge(feature_df, prevelance_df, left_index=True, right_index=True)


if __name__ == '__main__':
    ROOT_DIR = os.getcwd()

    L = len(getSubDirs())
    dfs = []
    for sim_dir, sim_dir_count in zip(getSubDirs(), range(L)):
        dframe = getSimDataFrameParallel(sim_dir)
        dfs.append(dframe)
        print(sim_dir_count + 1, 'out of', L, 'done.')

    print('Combining DataFrames')
    df = pd.concat(dfs)
    df = df.reset_index().drop('index', axis=1)

    print('Saving Result')
    os.chdir(ROOT_DIR)
    df.to_csv('data.csv')
