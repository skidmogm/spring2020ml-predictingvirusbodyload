import logging
import datetime
import os
from IPython.display import clear_output
import numpy as np
from numba import jit, prange
from IPython.display import Image
import time
import sys

# Set up things for logging
LOG_FILE = os.path.join(
    os.getcwd(), 'records.log')
logging.basicConfig(filename=LOG_FILE, level=logging.INFO)
ROOT_DIR = os.getcwd()

fileh = logging.FileHandler(LOG_FILE, 'a')
formatter = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
fileh.setFormatter(formatter)

log = logging.getLogger()  # root logger
for hdlr in log.handlers[:]:  # remove all old handlers
    log.removeHandler(hdlr)
log.addHandler(fileh)      # set the new handler

def update_progress(progress, name='Progress'):
    # Progress bar from https://www.mikulskibartosz.name/how-to-display-a-progress-bar-in-jupyter-notebook/
    bar_length = 20
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
    if progress < 0:
        progress = 0
    if progress >= 1:
        progress = 1

    block = int(round(bar_length * progress))

    clear_output(wait=True)
    text = "{2}: [{0}] {1:.1f}%".format(
        "#" * block + "-" * (bar_length - block), progress * 100, name)
    print(text)

# Constants
CELL_H   = 0
CELL_A2  = 1
CELL_A0  = 2
CELL_A1  = 4
CELL_D   = 8

@jit(nopython=True, parallel=True)
def step(grid, buffer, taugrid, P_inf, P_act, P_repl, tau1, tau2):
    for i in prange(1, grid.shape[0]-1):
        for j in range(1, grid.shape[1]-1):
            cell = grid[i,j]

            # (1) Rule for Healthy cells
            if cell == CELL_H:
                # If a healthy cell is in contact with at least one infected cell stage 1 (A1)
                # or at least R cells of infected cell stage 2 (A2),
                # (A) The healthy cell becomes an infected cell stage 1 (A1) with the probability P_inf.
                # (B) The healthy cell becomes a latently infected cell (A0) with the probability 1 - P_inf.
                neighbor_sum = ((grid[i-1,j-1] & 5) + (grid[i-1,j] & 5) + (grid[i-1,j+1] & 5) +
                                (grid[i,  j-1] & 5)                     + (grid[i,  j+1] & 5) +
                                (grid[i+1,j-1] & 5) + (grid[i+1,j] & 5) + (grid[i+1,j+1] & 5))
                if neighbor_sum >= 4:
                    if np.random.random() < P_inf:
                        buffer[i,j] = CELL_A1
                    else:
                        buffer[i,j] = CELL_A0

            # (2) Rule for infected cells stage 1
            elif cell == CELL_A1:
                # If an A1 cell has lived in the system for longer than tau1 time steps,
                # the A1 cell becomes an A2 cell. Otherwise, it remains the same state. 
                taugrid[i,j] += 1
                if taugrid[i,j] >= tau1:
                    taugrid[i,j] = 0
                    buffer[i,j] = CELL_A2

            # (3) Rule for infected cells stage 2
            elif cell == CELL_A2:
                # An A2 cell becomes a dead cell (D) at the following step.
                buffer[i,j] = CELL_D
            
            # (4) Rule for dead cells
            elif cell == CELL_D:
                # A dead cell (D) is replaced by a healthy cell with the probability P_repl.
                # Otherwise it remains unchanged with the probability 1 - P_repl. 
                if np.random.random() < P_repl:
                    buffer[i,j] = CELL_H
            
            # (5) Rule for latently infected cells
            else:
                # If an A0 cell has lived in the system for longer than tau2 time steps,
                # the A0 cell becomes an A1 cell with the probability P_act. Otherwise, it stays unchanged.
                if taugrid[i,j] < tau2: # Avoid overflow
                    taugrid[i,j] += 1
                if taugrid[i,j] >= tau2:
                    if np.random.random() < P_act:
                        taugrid[i,j] = 0
                        buffer[i,j] = CELL_A1
                        
@jit
def count(grid,w=1):
    healthy  = 0
    dead     = 0
    infected = 0
    latent   = 0
    for i in range(w, grid.shape[0]-w):
        for j in range(w, grid.shape[1]-w):
            cell = grid[i,j]
            if   cell == CELL_H:  healthy += 1
            elif cell == CELL_D:  dead += 1
            elif cell == CELL_A0: latent += 1
            else:                 infected += 1
    return healthy, infected, latent, dead

@jit
def init_grid(L, P_HIV, w=1):
    grid = np.zeros((L+w*2, L+w*2), dtype=np.uint8)

    for i in range(w, grid.shape[0]-w):
        for j in range(w, grid.shape[1]-w):
            if np.random.random() < P_HIV:
                grid[i,j] = CELL_A1

    return grid, np.zeros_like(grid)

def periodic_borders(grid,w=1):
    grid[:,:w]  = grid[:,-w-w:-w]
    grid[:,-w:] = grid[:,  w:w+w]
    grid[:w, :] = grid[-w-w:-w,:]
    grid[-w:,:] = grid[w:w+w,  :]
    
def run(timesteps, replications, L, P_HIV=0.005, P_inf=0.999, P_act=0.0025, P_repl=0.99, tau1=4, tau2=30):
    """
    Runs the model.
    """
    
    # Record this current batch of run
    d = datetime.datetime.today()
    d_str = d.strftime('%m-%d-%Y %H:%M:%S').replace(':', '.')
    logging.info('{0:s},T={1:d},Replications={2:d},L={3:d},P_HIV={4:s},P_inf={5:s},P_act={6:s},'
                 'P_repl={7:s},tau1={8:s},tau2={9:s}'.format(
                     d_str, timesteps, replications, L,
                     '{:f}'.format(P_HIV).rstrip('0'),
                     '{:f}'.format(P_inf).rstrip('0'),
                     '{:f}'.format(P_act).rstrip('0'),
                     '{:f}'.format(P_repl).rstrip('0'),
                     str(tau1),
                     str(tau2)))

    # Create new directory to store simulation results.
    # Name the new directory based on the date's string representation
    os.mkdir(d_str)
    os.chdir(d_str)
    
    # Dump a txt file in this directory also for further information
    with open('params.txt', 'w') as f:
        f.write('Simulation started at ' + d_str + '.\n\n')
        f.write('Parameters:\n')
        f.write('\tTimesteps:    {:d}\n'.format(timesteps))
        f.write('\tReplications: {:d}\n'.format(replications))
        f.write('\tGrid size:    {:d}\n'.format(L))
        f.write('\tP_HIV:        {:s}\n'.format(
            '{:f}'.format(P_HIV).rstrip('0')))
        f.write('\tP_inf:          {:s}\n'.format(
            '{:f}'.format(P_inf).rstrip('0')))
        f.write('\tP_act:          {:s}\n'.format(
            '{:f}'.format(P_act).rstrip('0')))
        f.write('\tP_repl:       {:s}\n'.format(
            '{:f}'.format(P_repl).rstrip('0')))
        f.write('\ttau1:          {:s}\n'.format(
            str(tau1)))
        f.write('\ttau2:          {:s}\n'.format(
            str(tau2)))

    update_progress(0)
    
    results_H = np.zeros((replications, timesteps))
    results_A = np.zeros((replications, timesteps))
    results_0 = np.zeros((replications, timesteps))
    results_D = np.zeros((replications, timesteps))
    
    # Add simulations storage for frames output
    simulations = [[] for i in range(replications)]
    frames = [[] for i in range(replications)]
    
    prevelance = np.zeros([replications, 4])
    
    for run in range(replications):
        # Allocate memory for all frames in the simulation
        npframes = np.zeros([timesteps + 1, L + 2, L + 2])
        
        grid, taugrid = init_grid(L, P_HIV)
        buffer = grid.copy()

        # Saves the state at t=0
        npframes[0, :, :] = grid[:, :]
        
        for t in range(timesteps):
            if(t == 0):
                frames[run].append([grid.copy()])
            elif(t == int(timesteps / 2)):
                frames[run].append([grid.copy()])
            elif(t == timesteps - 1):
                frames[run].append([grid.copy()])
            
            # Add updated frame (only to halfway point)
            if(t < timesteps / 2):
                simulations[run] += list(grid.copy())
            
            periodic_borders(grid)
            
            healthy, infected, latent, dead = count(grid)
            
            results_H[run,t] = healthy
            results_A[run,t] = infected
            results_0[run,t] = latent
            results_D[run,t] = dead
            
            step(grid, buffer, taugrid, P_inf, P_act, P_repl, tau1, tau2)
            grid[:,:] = buffer[:,:]
            
            npframes[t+1, :, :] = grid[:, :]
            
        # Dump file
        os.mkdir(str(run + 1))
        os.chdir(str(run + 1))
        for frame, num in zip(npframes, range(timesteps + 1)):
            np.savez_compressed(str(num), frame)
            update_progress((num + 1) / (timesteps + 1),
                            name='IO for run ' + str(run + 1))
        clear_output(wait=True)
        os.chdir(os.pardir)  # Exit

        # Saves prevelance of state
        prevelance[run, :] = np.array([healthy, infected, latent, dead])

        update_progress((run + 1) / replications)
        
    with open('prevelance.csv', 'w') as f:
        f.write('H,A,0,D\n')
        for row in prevelance:
            f.write(str(row[0]) + ',' + str(row[1]) + ',' +
                    str(row[2]) + ',' + str(row[3]) + '\n')

    os.chdir(os.pardir)
    print('Simulation results saved under the directory "' + d_str + '".')

    # Globally store the most recent, final results
    global resultsH, resultsA, resultsD, results0, simulationsG
    resultsH = results_H[replications - 1, timesteps - 1]
    resultsA = results_A[replications - 1, timesteps - 1]
    resultsD = results_D[replications - 1, timesteps - 1]
    results0 = results_0[replications - 1, timesteps - 1]
    simulationsG = simulations
    
    global picture
    picture = frames
    
    os.chdir(ROOT_DIR)
            
#     return results_H 
#results_A, results_0, results_D

