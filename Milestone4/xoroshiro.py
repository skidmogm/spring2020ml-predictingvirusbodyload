from numba import jit, float32, float64, uint8, uint32, uint64

import numpy as np

# Most of this code was adopted from https://github.com/numba/numba/blob/master/numba/cuda/random.py
# Numba uses the xoroshiro PRNG for doing Cuda operations.
# This code was extracted to avoid Cuda dependencies

@jit
def xoroshiro128p_init(seed):
    seed = uint64(seed)

    # SplitMix64
    z = seed + uint64(0x9E3779B97F4A7C15)
    z = (z ^ (z >> uint32(30))) * uint64(0xBF58476D1CE4E5B9)
    z = (z ^ (z >> uint32(27))) * uint64(0x94D049BB133111EB)
    z = z ^ (z >> uint32(31))

    s = np.empty(2, dtype=np.uint64)
    s[0] = z
    s[1] = z

    return s

@jit
def rotl(x, k):
    x = uint64(x)
    k = uint32(k)
    return (x << k) | (x >> uint32(64 - k))

@jit
def xoroshiro128p_next(s):
    s0 = s[0]
    s1 = s[1]
    result = s0 + s1

    s1 ^= s0
    s[0] = uint64(rotl(s0, uint32(55))) ^ s1 ^ (s1 << uint32(14))
    s[1] = uint64(rotl(s1, uint32(36)))

    return result

XOROSHIRO128P_JUMP = (uint64(0xbeac0467eba5facb), uint64(0xd86b048b86aa9922))

@jit
def xoroshiro128p_jump(s):
    s0 = uint64(0)
    s1 = uint64(0)

    for i in range(2):
        for b in range(64):
            if XOROSHIRO128P_JUMP[i] & (uint64(1) << uint32(b)):
                s0 ^= s[0]
                s1 ^= s[1]
            xoroshiro128p_next(s)

    s[0] = s0
    s[1] = s1

@jit
def uint64_to_unit_float64(x):
    x = uint64(x)
    return (x >> uint32(11)) * (float64(1) / (uint64(1) << uint32(53)))

@jit
def uint64_to_unit_float32(x):
    x = uint64(x)
    return float32(uint64_to_unit_float64(x))

@jit
def xoroshiro128p_uniform_float32(s):
    return uint64_to_unit_float32(xoroshiro128p_next(s))

@jit
def xoroshiro128p_uniform_float64(s):
    return uint64_to_unit_float64(xoroshiro128p_next(s))
