'''
One of thirty-two versions of the dosSantos CA model. Original model 
was not efficient enough to use realistic numbers of cells (billions); 
 combinations of five optimization methods were used to increase the 
 lattice size that could be updated in 10 minutes. 

The following optimizations were used in this script:
Numba's @jit decorator:    True
Parallel processing:       True
Xoroshiro_128 RNG:         True
Swapping memory addresses: True
Addition-based rules:      True
Create Plots:              False
'''



import numpy as np


from numba import jit,njit,prange





from xoroshiro import *
import time


P_HIV = 0.05
P_i = 0.997
P_v = 0.00001
P_rep = 0.99
P_repI = 0.00001
X = 4
tau1 = 4
tau2 = 1

# Each variable here is a different cell state; in dosSantos
# they represent healthy, dead, virulent infected and latent infected

CELL_H = 0
CELL_D = 1
CELL_I1 = 44
CELL_I2 = 11


def CA_SIM(n):
    '''Initializes lattices and random seeds, then begins the
	simulation with CA_HIV. Does not return a value.'''
    grid = np.full((n, n),fill_value = CELL_H, dtype='uint8')
    taugrid = np.zeros_like(grid, dtype='uint8')
    
    
    xoros = np.array([xoroshiro128p_init(np.random.randint(low=0,high=18446744073709551610,dtype=np.uint64)) for _ in range(n)])
    
    
    infect(grid, n, xoros)
    CA_HIV(grid, taugrid, n,xoros)



@jit(nopython=True, parallel=True)

def infect(N, n, xoros):
    '''Seeds a new, healthy grid with infectedA1 cells.
	Does not return a value.'''

    for i in prange(1, n - 1):
        for j in range(1, n - 1):
            if xoroshiro128p_uniform_float64(xoros[i]) <= P_HIV:
                N[i, j] = CELL_I1


@njit
def CA_HIV(grid, taugrid, n,xoros):
    '''Updates a given lattice 600 times using the rules in step().
	Does not return a value.'''
    buffer = grid.copy()
    for t in range(600):
        


        step(grid=grid, buffer=buffer, taugrid=taugrid,n= n,xoros=xoros)
        
        grid, buffer = buffer, grid
        

@jit(nopython=True, parallel=True)
def step(grid, buffer, taugrid, n,xoros):
    '''Updates buffer using contents of grid. Does not return a value.'''
    
    for i in prange(1, n - 1):
    
        for j in range(1, n - 1):
            value = grid[i, j]
            if value == CELL_H:
                
                    neighbor_sum = (np.uint16(grid[i - 1, j - 1]) + np.uint16(grid[i - 1, j]) + np.uint16(grid[i - 1, j + 1]) +
                                np.uint16(grid[i, j - 1]) + np.uint16(grid[i, j + 1]) +
                                np.uint16(grid[i + 1, j - 1]) + np.uint16(grid[i + 1, j]) + np.uint16(grid[i + 1, j + 1]))
                    if (neighbor_sum >= CELL_I1 and xoroshiro128p_uniform_float64(xoros[i]) <= P_i) or xoroshiro128p_uniform_float64(xoros[i]) <= P_v:
                        buffer[i, j] = CELL_I1
                    
                    else:
                        buffer[i,j] = CELL_H
                    
                

            elif value == CELL_I1:
                taugrid[i, j] += 1
                if taugrid[i, j] == tau1:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_I2
                
                else:
                    buffer[i,j] = CELL_I1
                

            elif value == CELL_I2:
                buffer[i, j] = CELL_D

            elif value == CELL_D:
                if xoroshiro128p_uniform_float64(xoros[i]) <= P_rep:
                    if xoroshiro128p_uniform_float64(xoros[i]) <= P_repI:
                        buffer[i, j] = CELL_I1
                    else:
                        buffer[i, j] = CELL_H
                
                else:
                    buffer[i,j] = CELL_D
                
            #else:
            #    raise Exception("ERROR!")

