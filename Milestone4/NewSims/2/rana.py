import os

import numpy as np
from numba import jit, prange

# Constants for the model
CELL_H = 0
CELL_Ht1 = 2  # therapy 1
CELL_Ht2 = 8  # therapy 2
CELL_HtB = 10  # dual therapy
CELL_D = 16
CELL_A1 = 4
CELL_A2 = 1


# Step functions for the model
# 1) non-therapy step
# 2) therapy step

@jit(nopython=True, parallel=True)
def m3Adh_step(grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau1):
    # variables to count the number of cells in different states
    inf_A1 = 0
    inf_A2 = 0
    dead = 0

    for i in prange(1, grid.shape[0] - 1):
        for j in range(1, grid.shape[1] - 1):
            cell = grid[i, j]

            if cell == CELL_H:

                # healthy cell can be infected by 1 A1 neighbor or 4 A2 neighbors (with a chance of resisting infection)
                if ((grid[i - 1, j - 1] & 5) + (grid[i - 1, j] & 5) + (grid[i - 1, j + 1] & 5) +
                    (grid[i, j - 1] & 5) + (grid[i, j + 1] & 5) +
                        (grid[i + 1, j - 1] & 5) + (grid[i + 1, j] & 5) + (grid[i + 1, j + 1] & 5)) >= 4:

                    if np.random.random() <= P_i:
                        buffer[i, j] = CELL_A1

                # or else the cell can be infected by a free-virion
                if np.random.random() <= P_v:
                    buffer[i, j] = CELL_A1

            elif cell == CELL_A1:
                inf_A1 += 1
                taugrid[i, j] += 1
                if taugrid[i, j] == tau1:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif cell == CELL_A2:
                inf_A2 += 1
                buffer[i, j] = CELL_D

            else:  # elif value == CELL_D:
                dead += 1
                randNum = np.random.random()

                if randNum < P_repH:
                    if randNum <= (P_repH * P_repI):
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_H

    return (grid.shape[0] - 2) * (grid.shape[1] - 2) - inf_A1 - inf_A2 - dead, 0, 0, 0, inf_A1, inf_A2, dead


@jit(nopython=True, parallel=True)
def m3Adh_tStep(grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau1, P_T1, P_infT1, P_T2, P_infT2, P_adh):
    # variables to count the number of cells in different states
    healthyT1 = 0
    healthyT2 = 0
    healthyTB = 0
    inf_A1 = 0
    inf_A2 = 0
    dead = 0

    for i in prange(1, grid.shape[0] - 1):
        for j in range(1, grid.shape[1] - 1):
            cell = grid[i, j]

            if cell == CELL_H:

                randNum1 = np.random.random()
                randNum2 = np.random.random()
                randNum3 = np.random.random()

                # Cell can receive therapy 1
                if (randNum1 <= P_T1) and (randNum2 > P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht1
                    continue

                # Cell can receive only therapy 2
                elif (randNum1 > P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht2
                    continue

                # Cell can receive both therapies
                elif (randNum1 <= P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_HtB
                    continue

                # healthy cell can be infected by 1 A1 neighbor or 4 A2 neighbors (with a chance of resisting infection)
                elif (((randNum1 <= P_i) and
                       (((grid[i - 1, j - 1] & 5) + (grid[i - 1, j] & 5) + (grid[i - 1, j + 1] & 5) +
                         (grid[i, j - 1] & 5) + (grid[i, j + 1] & 5) +
                         (grid[i + 1, j - 1] & 5) + (grid[i + 1, j] & 5) + (grid[i + 1, j + 1] & 5)) >= 4)) or
                      (randNum2 <= P_v)):

                    buffer[i, j] = CELL_A1
                    continue

            elif cell == CELL_Ht1:
                healthyT1 += 1

                randNum1 = np.random.random()
                randNum2 = np.random.random()
                randNum3 = np.random.random()

                # Cell can receive both therapies
                if (randNum1 <= P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_HtB
                    continue

                elif ((randNum1 <= P_infT1) and (((randNum1 <= P_i) and
                                                  (((grid[i - 1, j - 1] & 5) + (grid[i - 1, j] & 5) + (
                                                      grid[i - 1, j + 1] & 5) +
                                                    (grid[i, j - 1] & 5) + (grid[i, j + 1] & 5) +
                                                    (grid[i + 1, j - 1] & 5) + (grid[i + 1, j] & 5) + (
                                                      grid[i + 1, j + 1] & 5)) >= 4)) or
                                                 (randNum2 <= P_v))):

                    buffer[i, j] = CELL_A1
                    continue

                # Cell can receive only therapy 2
                elif (randNum1 > P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht2
                    continue

                # or else cell become normal healthy
                elif (randNum1 > P_T1 and randNum2 > P_T2):
                    buffer[i, j] = CELL_H

            elif cell == CELL_Ht2:
                healthyT2 += 1

                randNum1 = np.random.random()
                randNum2 = np.random.random()
                randNum3 = np.random.random()

                # Cell can receive both therapies
                if (randNum1 <= P_T1) and (randNum2 <= P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_HtB
                    continue

                elif ((randNum1 <= P_infT2) and (((randNum1 <= P_i) and
                                                  (((grid[i - 1, j - 1] & 5) + (grid[i - 1, j] & 5) + (
                                                      grid[i - 1, j + 1] & 5) +
                                                    (grid[i, j - 1] & 5) + (grid[i, j + 1] & 5) +
                                                    (grid[i + 1, j - 1] & 5) + (grid[i + 1, j] & 5) + (
                                                      grid[i + 1, j + 1] & 5)) >= 4)) or
                                                 (randNum2 <= P_v))):

                    buffer[i, j] = CELL_A1
                    continue

                # Cell can receive therapy 1
                elif (randNum1 <= P_T1) and (randNum2 > P_T2) and (randNum3 <= P_adh):
                    buffer[i, j] = CELL_Ht1
                    continue

                # or else cell becomes normal healthy
                elif (randNum1 > P_T1 and randNum2 > P_T2):
                    buffer[i, j] = CELL_H

            elif cell == CELL_HtB:
                healthyTB += 1
                buffer[i, j] = CELL_H

            elif cell == CELL_A1:
                inf_A1 += 1
                taugrid[i, j] += 1
                if taugrid[i, j] == tau1:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif cell == CELL_A2:
                inf_A2 += 1
                buffer[i, j] = CELL_D

            else:  # elif value == CELL_D:
                dead += 1
                randNum = np.random.random()

                if randNum < P_repH:
                    if randNum <= (P_repH * P_repI):
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_H

    return (grid.shape[0] - 2) * (grid.shape[1] - 2) - healthyT1 \
        - healthyT2 - healthyTB - inf_A1 - inf_A2 - dead, \
        healthyT1, healthyT2, healthyTB, inf_A1, inf_A2, dead


def run(file_name, timesteps=600, beginTherapy=20, P_adh=1.0, replications=1000, L=100, P_HIV=0.05, P_i=0.997,
        P_v=.00001, P_repH=.99, P_repI=.00001, tau=4, P_T1=0.7, P_infT1=0.07, P_T2=0.5, P_infT2=0.05):
    """
    Runs the model.
    """

    @jit
    def init_grid():
        grid = np.zeros((L + 2, L + 2), dtype=np.uint8)

        for i in range(1, grid.shape[0] - 1):
            for j in range(1, grid.shape[1] - 1):
                if np.random.random() <= P_HIV:
                    grid[i, j] = CELL_A1

        return grid, np.zeros_like(grid)

    # Prepare memory to store the prevelances of cell types (healthy
    # or dead, etc) at the t=600 stage for every replication of the
    # simulation.
    # prevelance = np.zeros([replications, 7])

    file = open(file_name, 'a')

    for run in range(replications):
        # Allocate memory for all frames in the simulation
        # Will be saved to disk later.
        frames = np.zeros([timesteps + 1, L + 2, L + 2])

        grid, taugrid = init_grid()
        buffer = grid.copy()

        # Saves the state at t=0
        frames[0, :, :] = grid[:, :]

        for t in range(timesteps):
            # if random value is greater than P_adh, no therapy is taken
            if t < beginTherapy:
                healthy, healthy_t1, healthy_t2, healthy_tb, inf_A1, inf_A2, dead = m3Adh_step(
                    grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau)

            # else the therapy is taken
            else:
                healthy, healthy_t1, healthy_t2, healthy_tb, inf_A1, inf_A2, dead = m3Adh_tStep(
                    grid, buffer, taugrid, P_i, P_v, P_repH, P_repI, tau, P_T1, P_infT1, P_T2, P_infT2, P_adh)

            grid[:, :] = buffer[:, :]

            # Saves the current grid to memory
            frames[t + 1, :, :] = grid[:, :]

        # Depending on the run, change to correct directory to save files
        SAVE_DIR = os.path.join(MODEL_ROOT_DIR, str(L), str(run + 1))
        if not os.path.exists(SAVE_DIR):
            os.makedirs(SAVE_DIR)

        os.chdir(SAVE_DIR)
        for frame, num in zip(frames, range(timesteps + 1)):
            np.savez_compressed(str(num), frame)

        # Saves prevelance of states to memory
        # COLUMNS = ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp',
        #            'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']
        data = ['Rana', run, L, healthy, healthy_t1, healthy_t2, healthy_tb, '', '', '',
                '', dead, '', inf_A1, inf_A2]
        data = list(map(lambda x: str(x), data))
        file.write(','.join(data) + '\n')


COLUMNS = ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp',
           'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']

# From where you start the script
ROOT_DIR = os.getcwd()
MODEL_ROOT_DIR = os.path.join(ROOT_DIR, 'rana')


def main(grid_sizes, replications):
    # Check if the root directory for the model exists
    # If not, create a new directory
    if not os.path.exists(MODEL_ROOT_DIR):
        os.mkdir(MODEL_ROOT_DIR)

    os.chdir(MODEL_ROOT_DIR)
    file_name = os.path.join(MODEL_ROOT_DIR, "ranaout.csv")

    # Create the output file when the file is not there so that
    # we do not write the header multiple times
    if not os.path.exists(file_name):
        f = open(file_name, 'a')
        f.write(','.join(COLUMNS) + '\n')
        f.close()

    # Run and output the simulations
    for L in grid_sizes:
        os.chdir(MODEL_ROOT_DIR)
        run(file_name, L=L, replications=replications, beginTherapy=2)
