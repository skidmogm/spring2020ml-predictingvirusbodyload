for time in ['2', '4', '8']:
    for model in ['rana', 'gonzalez']:
        for L in ['800', '1000', '1200']:
            with open(time + '/' + model + L + '.job', 'w') as f:
                f.writelines([
                    '#!/bin/bash -l\n',
                    '#PBS -N ' + model + L + '\n',
                    '#PBS -l nodes=1:ppn=4\n',
                    '#PBS -l walltime=48:00:00\n',
                    '#PBS -q batch\n',
                    '#PBS -m abe\n',
                    '#PBS -M lij111@miamioh.edu\n',
                    '#PBS -j oe\n',
                    '\n',
                    'cd /shared/giabbapj_shared/spring2020ml-predictingvirusbodyload/Milestone4/NewSims/{}/\n'.format(
                        time),
                    'source /software/python/anaconda3/etc/profile.d/conda.sh\n',
                    '$CONDA_PYTHON_EXE ' + model + L + '.py'
                ])
