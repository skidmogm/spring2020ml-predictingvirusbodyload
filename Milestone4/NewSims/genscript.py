for time in ['2', '4', '8']:
    for model in ['rana', 'gonzalez']:
        for L in ['800', '1000', '1200']:
            fname = '{}/{}{}.py'.format(time, model, L)
            with open('script.py') as fin, open(fname, 'w') as fout:
                for line in fin:
                    if line.strip() == "models = ['rana', 'gonzalez']":
                        fout.write('models = [\'{}\']\n'.format(model))
                    elif line.strip() == 'Ls = [800, 1000, 1200]':
                        fout.write('Ls = [{}]\n'.format(L))
                    elif line.strip() == 'f = open(model + "_grid_features.csv", \'w\')':
                        # We indented by spaces
                        fout.write('        f = open(model + "_{}_grid_features.csv", "w")\n'.format(L))
                    else:
                        fout.write(line)
