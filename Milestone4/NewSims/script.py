import numpy as np
import cv2
from numba import jit, prange
from skimage import feature as skif
from skimage.feature import blob_log
from concurrent.futures import ProcessPoolExecutor

# This is a script that first applies uniform cell states to each model.
# Then it uses four feature functions to extract a combination
# Dictionaries below have an original raw number(key), paired with the new uniform
# number(value).

# 1=healthy, 2=acute infected, 3=latent infected, 4=dead.

# Dicitonary to convert Dos Santos model to uniform states. 0:1(healthy),
# 4:2(acute infected), 1:3(latent infected), 8:4(dead)
doss_dict = {0: 1, 4: 2, 1: 3, 8: 4}

# Dictionary to convert Moonchai model to uniform states. 4:1(healthy),
# 2:2(acute infected), 1:3(latent infected), 8:4(dead) 0:0 (nonactivated)
moonchai_dict = {0: 0, 4: 1, 2: 2, 1: 3, 8: 4}

# Dictionary to convert Precharattana model to uniform states. 0:1(healthy),
# 4:2(acute infected), 1:2(acute infected), 2:3(latent infected), 8:4(dead)
precharattana_dict = {0: 1, 4: 2, 1: 2, 2: 3, 8: 4}

# Dictionary to convert the Rana model to uniform states. 0:1(healthy),
# 2:1(healthy), 8:1(healthy), 10:1(healthy), 4:2(acute infected),
# 1:3(latent infected), 16:4(dead)
rana_dict = {0: 1, 2: 1, 8: 1, 10: 1, 4: 2, 1: 3, 16: 4}

# Dictionary to convert the Gonzalez model to uniform states. 0:1(healthy),
# 1:1(healthy), 2:1(healthy), 3:1(healthy), 84:2(acute infected),
# 21:3(latent infected), 4:4(dead)
goz_dict = {0: 1, 1: 1, 2: 1, 3: 1, 84: 2, 21: 3, 4: 4}

# Defining variables for use later. Ls = list of grid sizes, replications =
# integer representing 300 unique runs, feature_names = list of the features to
# extract, models = list of models we'll run.
Ls = [800, 1000, 1200]
replications = 300
feature_names = ["5_num_blobs", "5_avg_blob_dist", "5_avg_blob_size",
                 "10_num_blobs", "10_avg_blob_dist", "10_avg_blob_size",
                 'cnt_area_10', 'cnt_area_100', 'cnt_perim_10',
                 'cnt_perim_20', 'avg_area_10', 'avg_area_100',
                 'avg_perim_10', 'avg_perim_20', 'max_cnt_area',
                 'max_cnt_perim', 'numofLogBlob', 'AvgSizeofLogblob',
                 'blob_varx', 'blob_vary', "Avg Manhatten Dist Between Blobs",
                 "Avg Euclidean Dist Between Blobs", "Num Corners",
                 "Avg Manhatten Dist Between Corners",
                 "Avg Euclidean Dist Between Corners"]
models = ['rana', 'gonzalez']

# Numerically encoded cell states, after being processed by
# the master script.
CELL_N = 0  # Non activated
CELL_H = 1  # Healthy
CELL_A1 = 2  # Acute Infected
CELL_A2 = 3  # Latent Infected
CELL_D = 4  # Dead

# Self-defined color map. Used for openCV feature-extraction functions.
mycmap = {
    CELL_N: [127, 127, 127],
    CELL_H: [0, 255, 0],
    CELL_A1: [255, 0, 0],
    CELL_A2: [0, 0, 255],
    CELL_D: [0, 0, 0],
}


# Helper function that takes a grid and increases the contrast
def adjust_array(array):
    max_val = np.max(array)
    return np.multiply(array, (255 / max_val))


# Helper function that takes a grid and returns an image operable by openCV.
@jit(nopython=True)
def gridToOpenCVImage(grid):
    # Create a L by L by 3 np array
    shape = grid.shape
    res = np.zeros((shape[0], shape[1], 3), dtype=np.uint8)

    CELL_N_ARR = np.array([127, 127, 127], dtype=np.uint8)
    CELL_H_ARR = np.array([0, 255, 0], dtype=np.uint8)
    CELL_A1_ARR = np.array([255, 0, 0], dtype=np.uint8)
    CELL_A2_ARR = np.array([0, 0, 255], dtype=np.uint8)
    CELL_D_ARR = np.array([0, 0, 0], dtype=np.uint8)

    # Note that openCV stores image in BGR format. The colors
    # in mycmap are given in RGB.
    for row in prange(len(grid)):
        for col in prange(len(grid[0])):
            # Gets the color in mycmap and reverse it to BGR
            value = grid[row][col]

            if value == CELL_N:
                res[row][col] = CELL_N_ARR[::-1]
            elif value == CELL_H:
                res[row][col] = CELL_H_ARR[::-1]
            elif value == CELL_A1:
                res[row][col] = CELL_A1_ARR[::-1]
            elif value == CELL_A2:
                res[row][col] = CELL_A2_ARR[::-1]
            else:
                res[row][col] = CELL_D_ARR[::-1]
    return res


# Helper function to run the SimpleBlobDetector algorithm on a given image.
# Return all found blobs with minimum area selected by input.
def getKeypoints(im, area):
    params = cv2.SimpleBlobDetector_Params()
    params.minThreshold = 2
    params.maxThreshold = 200
    params.filterByArea = True
    params.minArea = area
    params.filterByCircularity = False
    params.filterByConvexity = False
    params.filterByInertia = False
    detector = cv2.SimpleBlobDetector_create(params)
    return detector.detect(im)


# This function takes a grid as input and finds blob features. Returns a dictionary.
def getBlobFeatures(grid):
    # Helper function to compute the average distance among blobs
    @jit(nopython=True, parallel=True)
    def getAvgDistanceHelper(xList, yList):
        N = len(xList)
        res = 0
        for i in prange(N):
            for j in prange(i + 1, N):
                res += np.sqrt((xList[i] - xList[j]) **
                               2 + (yList[i] - yList[j]) ** 2)
        return 2 * res / (N ** 2 - N)

    def getAvgDistance(keypoints):
        if len(keypoints) <= 1:
            return 0

        xList, yList = list(zip(*[kp.pt for kp in keypoints]))
        xList = np.array(xList)
        yList = np.array(yList)
        return getAvgDistanceHelper(xList, yList)

    # Helper function to get the average size of the blobs
    def getAvgSize(keypoints):
        if keypoints == []:
            return 0
        return np.average([kp.size for kp in keypoints])

    # Extract image features and return them
    # keypoints_5 = getKeypoints(gridToOpenCVImage(grid), 5)
    keypoints_10 = getKeypoints(gridToOpenCVImage(grid), 10)

    return {
        "5_num_blobs": '',
        "5_avg_blob_dist": '',
        "5_avg_blob_size": '',
        "10_num_blobs": len(keypoints_10),
        "10_avg_blob_dist": getAvgDistance(keypoints_10),
        "10_avg_blob_size": getAvgSize(keypoints_10)
    }


# This function takes an openCV operable image as input and finds the contour perimeter and area. Returns a dictionary.
def getContourFeatures(im):
    # Grabs all contour edges from an image and calculates the perimeter of the contours
    def contour_perim(im, min_perim):
        imOrig = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imOrig, 127, 255, 0)
        contours, hierarchy = cv2.findContours(
            thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # First contour is the whole image, so it is removed
        contours = contours[1:]

        cnt_perim = np.array(
            list(map(lambda x: cv2.arcLength(x, True), contours)))
        cnt_perim_filtered = cnt_perim[cnt_perim > min_perim]
        return cnt_perim_filtered if cnt_perim_filtered.size > 0 else [0]

    # Grabs all contour edges from an image and calculates the area of the contours
    def contour_area(im, min_area):
        imOrig = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(imOrig, 127, 255, 0)
        contours, hierarchy = cv2.findContours(
            thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # First contour is the whole image, so it is removed
        contours = contours[1:]

        cnt_area = np.array(list(map(lambda x: cv2.contourArea(x), contours)))
        cnt_area_filtered = cnt_area[cnt_area > min_area]
        return cnt_area_filtered if cnt_area_filtered.size > 0 else [0]

    # Returns a dictionary of contour features.
    im = gridToOpenCVImage(im)
    return {
        'cnt_area_10': len(contour_area(im, 10)), 'cnt_area_100': len(contour_area(im, 100)),
        'cnt_perim_10': len(contour_perim(im, 10)), 'cnt_perim_20': len(contour_perim(im, 20)),
        'avg_area_10': np.mean(contour_area(im, 10)), 'avg_area_100': np.mean(contour_area(im, 100)),
        'avg_perim_10': np.mean(contour_perim(im, 10)), 'avg_perim_20': np.mean(contour_perim(im, 20)),
        'max_cnt_area': max(contour_area(im, 0)), 'max_cnt_perim': max(contour_perim(im, 0))
    }


# This function takes a grid as input and gets the xvar and yvar. Returns a dictionary.
def getXvarYvar(grid):
    def getBlobs(data):
        img = data
        blobs_log = blob_log(img, min_sigma=10, max_sigma=30,
                             num_sigma=10, threshold=0.5)
        return blobs_log

    def getBlobSize(blobs):
        return np.mean(blobs[:, 2])

    def getVar_x(blobs):
        return np.var(blobs[:, 0]) / len(blobs)

    def getVar_y(blobs):
        return np.var(blobs[:, 1]) / len(blobs)

    grid = adjust_array(grid)
    keypoints = getBlobs(grid)
    if len(keypoints) == 0:
        return {'numofLogBlob': len(keypoints), 'AvgSizeofLogblob': 0,
                'blob_varx': 0, 'blob_vary': 0}
    return {'numofLogBlob': len(keypoints), 'AvgSizeofLogblob': getBlobSize(keypoints),
            'blob_varx': getVar_x(keypoints), 'blob_vary': getVar_y(keypoints)}


# This function takes a grid as input and gets the corners, and avg manhatten and euclidean distances.
# Returns a dictionary.
def getCornersManEuc(grid):
    # Function that returns the average euclidian distance.
    @jit(nopython=True, parallel=True)
    def total_euclidean_distance(x_coords, y_coords, n):
        total = 0
        interactions = 0

        for i in prange(n):
            for j in prange(n):
                total += np.sqrt((x_coords[i] - x_coords[j])
                                 ** 2 + (y_coords[i] - y_coords[j]) ** 2)
                interactions += 1

        if interactions == 0:
            return 0
        else:
            return total / interactions

    # Function that returns the average manhattan distance.
    @jit(nopython=True)
    def total_manhatten_distance(x_coords, y_coords, n):
        total = 0
        interactions = 0

        for i in prange(n):
            for j in prange(n):
                total += (abs(x_coords[i] - x_coords[j]) +
                          abs(y_coords[i] - y_coords[j]))
                interactions += 1

        if interactions == 0:
            return 0
        else:
            return total / interactions

    #     # Helper function that gets the blob features
    #     def blob_features(array):
    #         blobs = skif.blob_doh(array)
    #         num_blobs = len(blobs)

    #         # collect the distances and size features
    #         x_coords = []
    #         y_coords = []
    #         for blob in blobs:
    #             x_coords.append(blob[1])
    #             y_coords.append(blob[0])

    #         avg_man_dist = total_manhatten_distance(x_coords, y_coords, num_blobs)
    #         avg_euc_dist = total_euclidean_distance(x_coords, y_coords, num_blobs)

    #         return avg_man_dist, avg_euc_dist

    def corner_features(array):
        # first, find potential corners, then find the most likely (or "peaks") of those likely corners
        corners = skif.corner_peaks(skif.corner_harris(array))

        num_corners = len(corners)

        x_coords, y_coords = zip(*[[x, y] for (x, y) in corners])

        x_coords = np.array(x_coords)
        y_coords = np.array(y_coords)

        avg_man_dist = total_manhatten_distance(
            x_coords, y_coords, num_corners)
        avg_euc_dist = total_euclidean_distance(
            x_coords, y_coords, num_corners)

        return num_corners, avg_man_dist, avg_euc_dist

    features = {}
    image = adjust_array(grid)
    # num_blobs, avg_radius, avg_man_dist_blobs, avg_euc_dist_blobs = blob_features(image)
    num_blobs, avg_radius, avg_man_dist_blobs, avg_euc_dist_blobs = 0, 0, 0, 0
    num_corners, avg_man_dist_corners, avg_euc_dist_corners = corner_features(
        image)

    # blob features in dict
    features["Avg Manhatten Dist Between Blobs"] = avg_man_dist_blobs
    features["Avg Euclidean Dist Between Blobs"] = avg_euc_dist_blobs

    # corner features in dict
    features["Num Corners"] = num_corners
    features["Avg Manhatten Dist Between Corners"] = avg_man_dist_corners
    features["Avg Euclidean Dist Between Corners"] = avg_euc_dist_corners
    return features


# Function that converts grids with raw values into a uniform set of values
# for all models.
@jit(nopython=True)
def im_convert(im, model):
    im_new = np.zeros((len(im), len(im[0])), dtype=np.uint8)
    if model == 'dossantos':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    im_new[row][col] = 1
                elif value == 4:
                    im_new[row][col] = 2
                elif value == 1:
                    im_new[row][col] = 3
                elif value == 8:
                    im_new[row][col] = 4
    elif model == 'moonchai':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    im_new[row][col] = 0
                elif value == 4:
                    im_new[row][col] = 1
                elif value == 2:
                    im_new[row][col] = 2
                elif value == 1:
                    im_new[row][col] = 3
                elif value == 8:
                    im_new[row][col] = 4
    elif model == 'precharattana':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    im_new[row][col] = 1
                elif value == 4:
                    im_new[row][col] = 2
                elif value == 1:
                    im_new[row][col] = 2
                elif value == 2:
                    im_new[row][col] = 3
                elif value == 8:
                    im_new[row][col] = 4
    elif model == 'rana':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    im_new[row][col] = 1
                elif value == 2:
                    im_new[row][col] = 1
                elif value == 8:
                    im_new[row][col] = 1
                elif value == 10:
                    im_new[row][col] = 1
                elif value == 4:
                    im_new[row][col] = 2
                elif value == 1:
                    im_new[row][col] = 3
                elif value == 16:
                    im_new[row][col] = 4
    elif model == 'gonzalez':
        for row in prange(0, len(im)):
            for col in prange(0, len(im[0])):
                value = im[row][col]
                if value == 0:
                    im_new[row][col] = 1
                elif value == 2:
                    im_new[row][col] = 1
                elif value == 8:
                    im_new[row][col] = 1
                elif value == 10:
                    im_new[row][col] = 1
                elif value == 4:
                    im_new[row][col] = 2
                elif value == 1:
                    im_new[row][col] = 3
                elif value == 16:
                    im_new[row][col] = 4
    return im_new


# Runs the main method when script.py is executed
if __name__ == '__main__':
    # Loops through each model in the directory, loads data for multiples of 5,
    # from 0-200. Calls feature extraction methods on that grid and writes them
    # to a csv output file.
    for model in models:
        # Opens a csv file for writing
        f = open(model + "_grid_features.csv", 'w')
        f.write("L,Run,T," + ",".join(feature_names) + '\n')
        f.flush()

        for L in Ls:
            for r in range(1, replications + 1):
                for i in range(0, 201):
                    data_path = model + "/" + \
                        str(L) + "/" + str(r) + "/" + str(i) + ".npz"
                    if (i % 5 == 0):
                        data = np.load(data_path)['arr_0']
                        im = data.copy()
                        im = im_convert(im, model)

                        futures = []
                        features = {}
                        with ProcessPoolExecutor(max_workers=4) as executor:
                            futures.append(
                                executor.submit(getBlobFeatures, im))
                            futures.append(executor.submit(
                                getContourFeatures, im))
                            futures.append(executor.submit(getXvarYvar, im))
                            futures.append(executor.submit(
                                getCornersManEuc, im))

                        for ftr in futures:
                            features.update(ftr.result())

                        feature = [str(L), str(r), str(i)]
                        for name in feature_names:
                            feature += [str(features[name])]
                        f.write(",".join(feature) + '\n')
                        f.flush()
        f.close()
