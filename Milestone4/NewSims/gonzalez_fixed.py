# Gonzalez's Combined Anti-retroviral Therapy Model

import numpy as np
from numba import jit, prange
import os

# CONSTANTS OF THE MODEL
# Cell states:

CELL_H = 0
CELL_H_rt = 1
CELL_H_p = 2
CELL_H_rtp = 3

CELL_D = 4

CELL_A1 = 84
CELL_A2 = 21


@jit(nopython=True, parallel=True)
def nonTherapyStep(grid, buffer, taugrid, P_reg, P_infec, tau):
    healthy = 0
    dead = 0

    for i in prange(1, grid.shape[0] - 1):
        for j in range(1, grid.shape[1] - 1):
            # Note: putting the 'rules' in a seperate function leads to a significant performance impact.
            value = grid[i, j]

            if value == CELL_H:
                neighbor_sum = (grid[i - 1, j - 1] + grid[i - 1, j] + grid[i - 1, j + 1] +
                                grid[i, j - 1] + grid[i, j + 1] +
                                grid[i + 1, j - 1] + grid[i + 1, j] + grid[i + 1, j + 1])

                if neighbor_sum >= CELL_A1:
                    buffer[i, j] = CELL_A1

            elif value == CELL_A1:
                taugrid[i, j] += 1
                if taugrid[i, j] == tau:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif value == CELL_A2:
                buffer[i, j] = CELL_D

            else:  # elif value == CELL_D:
                if np.random.random() < P_reg:
                    if np.random.random() < P_infec:
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_H
                # or else the cell remains dead

            if (value == CELL_H or value == CELL_H_rt or
                    value == CELL_H_p or value == CELL_H_rtp):
                healthy += 1
            elif (value == CELL_D):
                dead += 1

    return healthy, (grid.shape[0] - 2) * (grid.shape[1] - 2) - healthy - dead, dead


# # Step function for cART (both drug therapies)
@jit(nopython=True, parallel=True)
def rtpStep(grid, buffer, taugrid, P_reg, P_infec, tau, P_rti, P_pi):
    healthy = 0
    dead = 0

    for i in prange(1, grid.shape[0] - 1):
        for j in range(1, grid.shape[1] - 1):
            # Note: putting the 'rules' in a seperate function leads to a significant performance impact.
            value = grid[i, j]

            if value == CELL_H:
                if np.random.random() <= P_pi:
                    if np.random.random() <= P_rti:
                        buffer[i, j] = CELL_H_rtp
                    else:
                        buffer[i, j] = CELL_H_p
                else:
                    if np.random.random() <= P_rti:
                        buffer[i, j] = CELL_H_rt
                    else:
                        neighbor_sum = (grid[i - 1, j - 1] + grid[i - 1, j] + grid[i - 1, j + 1] +
                                        grid[i, j - 1] + grid[i, j + 1] +
                                        grid[i + 1, j - 1] + grid[i + 1, j] + grid[i + 1, j + 1])
                        if neighbor_sum >= CELL_A1:
                            buffer[i, j] = CELL_A1
                        else:
                            buffer[i, j] = CELL_H

            elif value == CELL_H_rt:
                if np.random.random() <= P_pi:
                    buffer[i, j] = CELL_H_rtp
                else:
                    if np.random.random() <= P_rti:
                        buffer[i, j] = CELL_H_rt
                    else:
                        neighbor_sum = (grid[i - 1, j - 1] + grid[i - 1, j] + grid[i - 1, j + 1] +
                                        grid[i, j - 1] + grid[i, j + 1] +
                                        grid[i + 1, j - 1] + grid[i + 1, j] + grid[i + 1, j + 1])
                        if neighbor_sum >= CELL_A1:
                            buffer[i, j] = CELL_A1
                        else:
                            buffer[i, j] = CELL_H

            elif value == CELL_H_p:
                if np.random.random() <= P_rti:
                    buffer[i, j] = CELL_H_rtp
                else:
                    if np.random.random() <= P_pi:
                        buffer[i, j] = CELL_H_p
                    else:
                        neighbor_sum = (grid[i - 1, j - 1] + grid[i - 1, j] + grid[i - 1, j + 1] +
                                        grid[i, j - 1] + grid[i, j + 1] +
                                        grid[i + 1, j - 1] + grid[i + 1, j] + grid[i + 1, j + 1])
                        if neighbor_sum >= CELL_A1:
                            buffer[i, j] = CELL_A1
                        else:
                            buffer[i, j] = CELL_H

            elif value == CELL_H_rtp:
                buffer[i, j] = CELL_H

            elif value == CELL_A1:
                taugrid[i, j] += 1
                if taugrid[i, j] == tau:
                    taugrid[i, j] = 0
                    buffer[i, j] = CELL_A2

            elif value == CELL_A2:
                buffer[i, j] = CELL_D

            else:  # elif value == CELL_D:
                if np.random.random() <= P_reg:
                    if np.random.random() <= P_infec:
                        buffer[i, j] = CELL_A1
                    else:
                        buffer[i, j] = CELL_H
                else:
                    buffer[i, j] = CELL_D

            if (value == CELL_H or value == CELL_H_rt or
                    value == CELL_H_p or value == CELL_H_rtp):
                healthy += 1
            elif (value == CELL_D):
                dead += 1

    return healthy, (grid.shape[0] - 2) * (grid.shape[1] - 2) - healthy - dead, dead


# eff() calculates the effectiveness of a treatment given:
#   1. a maximum effectiveness of the drug (P_0j)
#   2. a concentration of infected cells at initiation of treatment (D_I0)
#   3. a concentration of infected cells at a given time step (D_I)
#
# it returns a float between 0 and 1

@jit
def eff(P_0j, D_I0, D_I):
    return (P_0j * ((D_I / D_I0) ** ((2 * D_I0) / (1 - D_I0))) * ((1 - D_I) / (1 - D_I0)) ** 2)


def run(file_name, timesteps, replications, nonTherapySteps=300, drugTherapy=rtpStep, L=700, P_HIV=0.05, P_reg=0.99,
        P_infec=1e-05, tau=4, P_0j=0.5):
    """
    Runs the model.
    """

    # THIS IS WRONG! PROBABILITIES ARE NOT COMBINED LIKE THIS.
    # # Probability that a dead cell will be replaced by a healthy or infected cell is a function of P_infec and P_reg
    # P_repH = (1 - P_infec) * P_reg
    # P_repI = P_infec * P_reg

    @jit
    def init_grid():
        grid = np.zeros((L + 2, L + 2), dtype=np.uint8)

        for i in range(1, grid.shape[0] - 1):
            for j in range(1, grid.shape[1] - 1):
                if np.random.random() < P_HIV:
                    grid[i, j] = CELL_A1

        return grid, np.zeros_like(grid)

    def periodic_borders(grid):
        grid[1:-1, 0] = grid[1:-1, -2]
        grid[1:-1, -1] = grid[1:-1, 1]
        grid[0, 1:-1] = grid[-2, 1:-1]
        grid[-1, 1:-1] = grid[1, 1:-1]
        grid[0, 0] = grid[-2, -2]
        grid[0, -1] = grid[-2, 1]
        grid[-1, 0] = grid[1, -2]
        grid[-1, -1] = grid[1, 1]

    def save_array(grid, run, t):
        # will save under the file structure model_name/length/simulation_#/timestep
        file_name = "{}.npz".format(t)
        np.savez_compressed(file_name, arr_0=grid)

    def count_states(grid):
        CELL_H_count = 0
        CELL_H_rt_count = 0
        CELL_H_p_count = 0
        CELL_H_rtp_count = 0
        CELL_A1_count = 0
        CELL_A2_count = 0
        CELL_D_count = 0

        for i in prange(1, grid.shape[0] - 1):
            for j in range(1, grid.shape[1] - 1):
                value = grid[i, j]

                if value == CELL_H:
                    CELL_H_count += 1
                elif value == CELL_H_rt:
                    CELL_H_rt_count += 1
                elif value == CELL_H_p:
                    CELL_H_p_count += 1
                elif value == CELL_H_rtp:
                    CELL_H_rtp_count += 1
                elif value == CELL_A1:
                    CELL_A1_count += 1
                elif value == CELL_A2:
                    CELL_A2_count += 1
                elif value == CELL_D:
                    CELL_D_count += 1

        return CELL_H_count, CELL_H_rt_count, CELL_H_p_count, CELL_H_rtp_count, CELL_D_count, CELL_A1_count, CELL_A2_count

    results_H = np.zeros((replications, timesteps))
    results_A = np.zeros((replications, timesteps))
    results_D = np.zeros((replications, timesteps))

    file = open(file_name, 'a')

    for run in range(replications):
        grid, taugrid = init_grid()
        buffer = grid.copy()

        # Depending on the run, change to correct directory to save files
        SAVE_DIR = os.path.join(MODEL_ROOT_DIR, str(L), str(run + 1))
        if not os.path.exists(SAVE_DIR):
            os.makedirs(SAVE_DIR)

        os.chdir(SAVE_DIR)

        # Save the t=0 grid
        save_array(grid, run, 0)

        for t in range(timesteps):
            # print(t)
            periodic_borders(grid)

            if t < nonTherapySteps:
                healthy, infected, dead = nonTherapyStep(grid, buffer, taugrid, P_reg, P_infec, tau)

            else:
                if t == nonTherapySteps:
                    # Save the concentration of infected cells at step treatment begins
                    initC = results_A[run, t - 1] / ((L - 2) * (L - 2))
                    newT = t - 2
                    while initC == 0.0:
                        initC = results_A[run, newT] / ((L - 2) * (L - 2))
                        newT -= 1

                if t == 301:
                    pass

                tC = results_A[run, t - 1] / ((L - 2) * (L - 2))
                P_rti = eff(P_0j, initC, tC)
                P_pi = eff(P_0j, initC, tC)

                healthy, infected, dead = drugTherapy(grid, buffer, taugrid, P_reg, P_infec, tau, P_rti, P_pi)

            results_H[run, t] = healthy  # / ((L-2)*(L-2))
            results_A[run, t] = infected  # / ((L-2)*(L-2))
            results_D[run, t] = dead  # / ((L-2)*(L-2))

            # Save cell counts
            if t == timesteps - 1:
                # ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p',
                # 'CELL_H_rtp', 'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']
                CELL_H_count, CELL_H_rt_count, CELL_H_p_count, CELL_H_rtp_count, \
                CELL_D_count, CELL_A1_count, CELL_A2_count = count_states(grid)

                data = ['Gonzalez', str(run + 1), str(L), str(CELL_H_count), '', '', '', str(CELL_H_rt_count),
                        str(CELL_H_p_count), str(CELL_H_rtp_count), '',
                        str(CELL_D_count), '', str(CELL_A1_count), str(CELL_A2_count)]
                file.write(','.join(data) + '\n')
                file.flush()

            # We have propagated in time once ALREADY, this will be t+1.
            grid[:, :] = buffer[:, :]
            save_array(grid, run, t + 1)

    file.close()


COLUMNS = ['Model', 'Run', 'L', 'CELL_H', 'CELL_Ht1', 'CELL_Ht2', 'CELL_HtB', 'CELL_H_rt', 'CELL_H_p', 'CELL_H_rtp',
           'CELL_N', 'CELL_D', 'CELL_A0', 'CELL_A1', 'CELL_A2']

# From where you start the script
ROOT_DIR = os.getcwd()
MODEL_ROOT_DIR = os.path.join(ROOT_DIR, 'gonzalez')


def main(grid_sizes, replications):
    Ls = grid_sizes

    p_hivs = [0.05]
    p_repls = [0.99]
    p_infecs = [1e-05]

    tauls = [4]
    replications = replications
    timesteps = 600

    # Check if the root directory for the model exists
    # If not, create a new directory
    if not os.path.exists(MODEL_ROOT_DIR):
        os.mkdir(MODEL_ROOT_DIR)

    os.chdir(MODEL_ROOT_DIR)
    file_name = os.path.join(MODEL_ROOT_DIR, "gonzalezout.csv")

    # Create the output file when the file is not there so that
    # we do not write the header multiple times
    if not os.path.exists(file_name):
        f = open(file_name, 'a')
        f.write(','.join(COLUMNS) + '\n')
        f.close()

    # Run and output the simulations
    for L in Ls:
        for p_hiv in p_hivs:
            for p_repl in p_repls:
                for p_infec in p_infecs:
                    for taul in tauls:
                        os.chdir(MODEL_ROOT_DIR)
                        # run(file_name, timesteps, replications, nonTherapySteps=300, drugTherapy=rtpStep, L=700, P_HIV=0.05, P_reg=0.99,
                        #         P_infec=1e-05, tau=4, P_0j=0.5):
                        run(file_name, timesteps, replications, nonTherapySteps=300, drugTherapy=rtpStep, L=L,
                            P_HIV=p_hiv, P_reg=p_repl, P_infec=p_infec, P_0j=0.5, tau=taul)
